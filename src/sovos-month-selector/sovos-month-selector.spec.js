import React from 'react';
import { mount } from 'enzyme';
import SovosMonthSelector from './SovosMonthSelector';
import SovosThemeProvider from '../sovos-theme-provider/SovosThemeProvider';

const renderMonthSelector = ({
    minDate,
  maxDate,
  onChange
  }) =>
  mount(<SovosThemeProvider>
    <SovosMonthSelector
      minDate={ minDate }
      maxDate={ maxDate }
      onChange={ onChange }
    />
  </SovosThemeProvider>
  );

describe('SovosMonthSelector', () => {
  describe('default', () => {
    it('calls onChange when loaded', () => {
      const onChange = sinon.spy();
      renderMonthSelector({ onChange });

      expect(onChange).toHaveBeenCalled(1);
    });

    it('calls onChange when previous button is clicked', () => {
      const onChange = sinon.spy();
      const wrapper = renderMonthSelector({ onChange });
      wrapper.find('button .previous-button').simulate('click');
      expect(onChange).toHaveBeenCalled(2);
    });

    it('calls onChange when next button is clicked', () => {
      const onChange = sinon.spy();
      const wrapper = renderMonthSelector({ onChange });
      wrapper.find('button .next-button').simulate('click');
      expect(onChange).toHaveBeenCalled(2);
    });

    it('has date set to last month when prev button is pressed', () => {
      let output;
      const onChange = (o) => { output = o; };
      const wrapper = renderMonthSelector({ onChange });

      wrapper.find('button .previous-button').simulate('click');

      expect(output).toEqual({
        month: new Date().getMonth(),
        year: new Date().getFullYear()
      });
    });

    it('does nothing when next button is clicked and current selection is at maxDate', () => {
      const maxDate = new Date();
      maxDate.setMonth(new Date().getMonth() - 1);
      const onChange = sinon.spy();
      const wrapper = renderMonthSelector({ onChange, maxDate });

      wrapper.find('button .next-button').simulate('click');

      expect(onChange).toHaveBeenCalled(1);
    });

    it('does nothing when previous button is clicked and current selection is at minDate', () => {
      const minDate = new Date();
      minDate.setMonth(new Date().getMonth() + 1);
      const onChange = sinon.spy();
      const wrapper = renderMonthSelector({ onChange, minDate });

      wrapper.find('button .previous-button').simulate('click');

      expect(onChange).toHaveBeenCalled(1);
    });

    it('has date set to maxDate if maxDate is less than today', () => {
      let output;
      const onChange = (o) => { output = o; };
      const minDate = new Date(2010, 0);
      const maxDate = new Date(2010, 5);

      renderMonthSelector({ onChange, minDate, maxDate });

      expect(output).toEqual({
        month: 6,
        year: 2010
      });
    });

    it('formats the date to {monthName, year} when format is "month-name"', () => {
      let output;
      const onChange = (o) => { output = o; };

      renderMonthSelector({ onChange, selectedMonth: 1, format: 'month-name' });

      expect(output).toEqual({
        month: 'January',
        year: new Date().getFullYear()
      });
    });

    it('formats the date to {monthNumber, year} when format is "number"', () => {
      let output;
      const onChange = (o) => { output = o; };

      renderMonthSelector({ onChange, format: 'number' });

      expect(output).toEqual({
        month: new Date().getMonth() + 1,
        year: new Date().getFullYear()
      });
    });
  });
});
