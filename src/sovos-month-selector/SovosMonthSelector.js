import PropTypes from 'prop-types';
import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import Paper from 'material-ui/Paper';
import MenuItem from 'material-ui/MenuItem';
import Calendar from 'material-ui/svg-icons/action/date-range';
import Left from 'material-ui/svg-icons/hardware/keyboard-arrow-left';
import Right from 'material-ui/svg-icons/hardware/keyboard-arrow-right';
import SovosFlatButton from '../sovos-flat-button/SovosFlatButton';
import SovosIconButton from '../sovos-icon-button/SovosIconButton';
import { purple } from '../themes/sovos-colors';

const MonthsAbbreviated = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
const MonthsFull = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

class SovosMonthSelector extends React.Component {
  constructor(props) {
    super(props);

    const { selectedMonth, selectedYear, minDate, maxDate } = props;

    const year = Math.max(Math.min(selectedYear, maxDate.getFullYear()), minDate.getFullYear());
    let month = selectedMonth;
    if (year === minDate.getFullYear()) {
      month = Math.max(minDate.getMonth(), selectedMonth);
    } else if (year === maxDate.getFullYear()) {
      month = Math.min(maxDate.getMonth(), selectedMonth);
    }

    this.state = {
      showMenu: false,
      selectedMonth: month,
      selectedYear: year,
      isActive: false
    };
  }

  componentDidMount = () =>
    this.onValueChanged();

  onBlur = () =>
    setTimeout(() => this.toggleMenu(false), 10);

  onValueChanged = () => {
    if (this.props.onChange) {
      this.props.onChange(this.getOutput());
    }
  }

  setStateAndTriggerUpdateEvent = state =>
    this.setState(state, this.onValueChanged);

  getOutput = () => {
    switch (this.props.format) {
      case 'month-name':
        return {
          month: MonthsFull[this.state.selectedMonth],
          year: this.state.selectedYear
        };
      case 'number':
        return {
          month: this.state.selectedMonth + 1,
          year: this.state.selectedYear
        };
      default:
        throw new Error(`Unrecognized format '${this.props.format}' in SovosMonthSelector.`);
    }
  }

  toggleMenu = on =>
    this.setState(
      { showMenu: typeof (on) === 'boolean' ? on : !this.state.showMenu },
      () => { this.props.onToggle(this.state.showMenu); }
    );

  selectMonth = month =>
    this.setStateAndTriggerUpdateEvent({
      selectedMonth: month
    });

  selectYear = (year) => {
    const { minDate, maxDate } = this.props;
    const { selectedMonth } = this.state;
    const newState = { selectedYear: year };

    if (year === minDate.getFullYear()) {
      newState.selectedMonth = Math.max(minDate.getMonth(), selectedMonth);
    }

    if (year === maxDate.getFullYear()) {
      newState.selectedMonth = Math.min(maxDate.getMonth(), selectedMonth);
    }

    this.setStateAndTriggerUpdateEvent(newState);
  }

  canIncreaseMonth = () => {
    const { maxDate } = this.props;
    const { selectedYear, selectedMonth } = this.state;

    if (selectedYear !== maxDate.getFullYear()) { return true; }

    return selectedMonth < maxDate.getMonth();
  }

  canDecreaseMonth = () => {
    const { minDate } = this.props;
    const { selectedYear, selectedMonth } = this.state;

    if (selectedYear !== minDate.getFullYear()) { return true; }

    return selectedMonth > minDate.getMonth();
  }

  nextMonth = () => {
    if (!this.canIncreaseMonth()) { return; }

    const { selectedYear, selectedMonth } = this.state;
    const newMonth = selectedMonth + 1;
    const newState = { selectedMonth: newMonth };

    if (newMonth > 11) {
      newState.selectedYear = selectedYear + 1;
      newState.selectedMonth = 0;
    }

    this.setStateAndTriggerUpdateEvent(newState);
  }

  prevMonth = () => {
    if (!this.canDecreaseMonth()) { return; }

    const { selectedYear, selectedMonth } = this.state;
    const newMonth = selectedMonth - 1;
    const newState = { selectedMonth: newMonth };

    if (newMonth < 0) {
      newState.selectedYear = selectedYear - 1;
      newState.selectedMonth = 11;
    }

    this.setStateAndTriggerUpdateEvent(newState);
  }

  renderMonths = () => {
    const { minDate, maxDate, muiTheme: { monthSelector } } = this.props;
    const { selectedMonth, selectedYear } = this.state;

    return MonthsAbbreviated.map((item, index) => {
      const style = { ...monthSelector.menuItem };

      if (index === selectedMonth) {
        style.backgroundColor = purple;
        style.color = 'white';
      }

      const disabled =
        (selectedYear === minDate.getFullYear() && index < minDate.getMonth())
        || (selectedYear === maxDate.getFullYear() && index > maxDate.getMonth());

      return (<MenuItem
        key={ `month-${index}` }
        primaryText={ item }
        style={ style }
        disabled={ disabled }
        onClick={ disabled ? null : () => this.selectMonth(index) }
      />);
    });
  }

  renderYears = () => {
    const { minDate, maxDate, muiTheme: { monthSelector } } = this.props;

    const years = [];
    for (let year = minDate.getFullYear(); year <= maxDate.getFullYear(); year++) {
      years.push(year);
    }

    return years.map((item) => {
      const style = { ...monthSelector.menuItem };
      const isSelected = item === this.state.selectedYear;

      if (isSelected) {
        style.backgroundColor = purple;
        style.color = 'white';
      }

      return (<MenuItem
        key={ `year-${item}` }
        primaryText={ item }
        style={ style }
        onClick={ () => this.selectYear(item) }
      />);
    });
  }

  renderDropdownMenu = () => {
    const { muiTheme: { monthSelector } } = this.props;

    if (!this.state.showMenu) return null;

    return (
      <Paper style={ monthSelector.dropdown }>
        <div style={ monthSelector.menuSections.left } >
          {this.renderMonths()}
        </div>

        <div style={ monthSelector.menuSections.right } >
          {this.renderYears()}
        </div>
      </Paper>
    );
  }

  renderWidget = () => {
    const { muiTheme: { monthSelector } } = this.props;

    const today = new Date();
    const label = today.getMonth() === this.state.selectedMonth
      && today.getFullYear() === this.state.selectedYear
      ? 'This Month'
      : `${MonthsAbbreviated[this.state.selectedMonth]} ${this.state.selectedYear}`;

    return (
      <Paper style={ monthSelector.widget }>
        <SovosFlatButton
          className="month-select"
          type="custom"
          icon={ <Calendar /> }
          style={ monthSelector.currentSelectionButton }
          color={ purple }
          label={ label }
          onClick={ this.toggleMenu }
          onBlur={ this.onBlur }
          tooltip="Go to a specific month"
        />

        <SovosIconButton
          className="previous-button"
          color={ purple }
          style={ monthSelector.nextPrevButton }
          onClick={ this.prevMonth }
          tooltip="Previous Month"
          disabled={ !this.canDecreaseMonth() }
        >
          <Left />
        </SovosIconButton>

        <SovosIconButton
          className="next-button"
          color={ purple }
          style={ monthSelector.nextPrevButton }
          onClick={ this.nextMonth }
          tooltip="Next Month"
          disabled={ !this.canIncreaseMonth() }
        >
          <Right />
        </SovosIconButton>
      </Paper>
    );
  }

  render() {
    const { muiTheme: { monthSelector } } = this.props;

    return (
      <div style={ monthSelector.container } >
        {this.renderWidget()}
        {this.renderDropdownMenu()}
      </div>
    );
  }
}

SovosMonthSelector.propTypes = {
  muiTheme: PropTypes.object,
  onChange: PropTypes.func,
  onToggle: PropTypes.func,
  selectedMonth: PropTypes.number,
  selectedYear: PropTypes.number,
  minDate: PropTypes.instanceOf(Date),
  maxDate: PropTypes.instanceOf(Date),
  format: PropTypes.oneOf(['number', 'month-name'])
};

SovosMonthSelector.defaultProps = {
  onToggle: () => { },
  onValueChanged: () => { },
  minDate: new Date(new Date().getFullYear(), 0),
  maxDate: new Date(new Date().getFullYear(), 11),
  selectedYear: new Date().getFullYear(),
  selectedMonth: new Date().getMonth(),
  format: 'number'
};

export default muiThemeable()(SovosMonthSelector);
