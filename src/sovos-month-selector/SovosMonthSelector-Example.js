import React from 'react';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import SovosMonthSelector from './SovosMonthSelector';

const cardStyle = {
  display: 'inline-block',
  textAlign: 'left',
  flex: 1,
  margin: 10,
  zIndex: 2
};

const displayDate = (elementId, output) => {
  document.getElementById(elementId).innerHTML = JSON.stringify(output);
};

export default () => (
  <Card style={ { textAlign: 'center', margin: '1rem' } }>
    <CardTitle title="Month Selector" />
    <CardText style={ { display: 'flex' } }>

      <Card style={ cardStyle }>
        <CardText>
          <CardTitle>
            <div><b>Min-MaxDate:</b> 5/2012 - 5/2017</div>
            <div><b>Format:</b> "month-name"</div>
            <div><b>Output:</b> <span id="datePicker1" /></div>
          </CardTitle>
          <SovosMonthSelector
            minDate={ new Date(2012, 5) }
            maxDate={ new Date(2017, 5) }
            format="month-name"
            onChange={ date => displayDate('datePicker1', date) }
          />
        </CardText>
      </Card>

      <Card style={ cardStyle }>
        <CardText>
          <CardTitle>
            <div><b>Min-MaxDate:</b> Default</div>
            <div><b>Format:</b> Default</div>
            <div><b>Output:</b> <span id="datePicker2" /></div>
          </CardTitle>
          <SovosMonthSelector
            onChange={ date => displayDate('datePicker2', date) }
          />
        </CardText>
      </Card>
    </CardText>
  </Card>
);
