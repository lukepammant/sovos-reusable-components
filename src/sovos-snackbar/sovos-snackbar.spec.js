import React from 'react';
import { mount } from 'enzyme';
import SovosSnackbar from './SovosSnackbar';
import SovosThemeProvider from '../sovos-theme-provider/SovosThemeProvider';
import { blueDark, redMedium } from '../themes/sovos-colors';

describe('SovosSnackbar', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(
      <SovosThemeProvider>
        <SovosSnackbar
          message="test"
          open
          closeSovosSnackbar={ () => null }
        />
      </SovosThemeProvider>
    );
  });

  it('has Snackbar autoHideDuration set to 4000', () => {
    const container = wrapper.find('Snackbar').prop('autoHideDuration');
    expect(container).toEqual(4000);
  });

  it('has Snackbar backgroundColor set to blue', () => {
    const style = wrapper.find('Snackbar').prop('bodyStyle');
    expect(style.backgroundColor).toEqual(blueDark);
  });

  it('has Snackbar width set to 100%', () => {
    const style = wrapper.find('Snackbar').prop('style');
    expect(style.width).toEqual('100%');
  });
});

describe('SovosSnackbar error', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(
      <SovosThemeProvider>
        <SovosSnackbar
          snackBarType="error"
          message="test"
          open
          closeSovosSnackbar={ () => null }
        />
      </SovosThemeProvider>
    );
  });

  it('has Snackbar backgroundColor set to red', () => {
    const style = wrapper.find('Snackbar').prop('bodyStyle');
    expect(style.backgroundColor).toEqual(redMedium);
  });
});
