import PropTypes from 'prop-types';
import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import {Snackbar} from 'material-ui';

const CONFIRMATION = 'confirmation';
const WARNING = 'warning';
const ERROR = 'error';

class SovosSnackbar extends React.Component {
    constructor(props) {
        super(props);
    }

    handleOnRequestClose = (reason) => {
        if (reason !== 'clickaway') {
            this.props.closeSovosSnackbar();
        }
    };

    handleOnActionTouchTap = () => {
        this.props.actionSovosSnackbar();
    };

    getSnackBarWidth = () => {
        switch (this.props.length) {
            case 'short':
                return '300px';
                break;
            case 'medium':
                return '600px';
                break;
            case 'long':
            default:
                return '100%';
                break;
        }
    };

    getSnackBarColor = () => {
        const {backgroundColor} = this.props.muiTheme.snackbar;
        const {errorBackgroundColor} = this.props.muiTheme.snackbar;
        const {warningBackgroundColor} = this.props.muiTheme.snackbar;

        switch (this.props.snackBarType) {
            case CONFIRMATION:
                return backgroundColor;
                break;
            case WARNING:
                return warningBackgroundColor;
                break;
            case ERROR:
                return errorBackgroundColor;
                break;
            default:
                return backgroundColor;
                break;
        }
    };

    getSnackBarAction = () => {
        if (this.props.snackBarType === ERROR) {
            return "CLOSE";
        }
    };

    getAutoHideDuration = () => {
        if (this.props.snackBarType !== ERROR) {
            return 4000;
        }
    };

    render() {
        return <Snackbar
            style={{maxWidth: this.getSnackBarWidth(),
                    width:this.getSnackBarWidth(),
                    padding: '0px, 20px'}}
            bodyStyle={{maxWidth:this.getSnackBarWidth(),
                        width: this.getSnackBarWidth(),
                        height: '50px',
                        rightMargin: '30px',
                        backgroundColor: this.getSnackBarColor()}}
            open={this.props.open}
            message={this.props.message}
            autoHideDuration={this.getAutoHideDuration()}
            action={(this.props.actionLabel ?  this.props.actionLabel : this.getSnackBarAction())}
            onActionTouchTap={this.handleOnActionTouchTap}
            onRequestClose={this.handleOnRequestClose}
        />;
    }
}

SovosSnackbar.propTypes = Object.assign({}, Snackbar.propTypes, {
    open: PropTypes.bool,
    length: PropTypes.string,
    message: PropTypes.string,
    actionLabel: PropTypes.string,
    snackBarType: PropTypes.string,
    closeSovosSnackbar: PropTypes.func.isRequired,
    actionSovosSnackbar: PropTypes.func
});

export default muiThemeable()(SovosSnackbar);
