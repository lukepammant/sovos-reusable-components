import React from 'react';
import { mount } from 'enzyme';
import SovosRaisedButton from './SovosRaisedButton';
import SovosThemeProvider from '../sovos-theme-provider/SovosThemeProvider';
import Theme from '../themes/sovos-enterprise';

const renderRaisedButton = ({ type, backgroundColor, labelColor, onClick }) =>
  mount(
    <SovosThemeProvider>
      <SovosRaisedButton
        label="Testing"
        type={ type }
        backgroundColor={ backgroundColor }
        labelColor={ labelColor }
        onClick={ onClick }
      />
    </SovosThemeProvider>
  );

describe('SovosRaisedButton default', () => {
  let wrapper;
  let onClick;

  beforeEach(() => {
    onClick = sinon.spy();
    wrapper = renderRaisedButton({ onClick });
  });

  it('has Button backgroundColor set to "primaryButtonBackground" when "type" is not defined', () => {
    const style = wrapper.find('button').prop('style');
    expect(style.backgroundColor).toEqual(Theme.raisedButton.primary.backgroundColor);
  });

  it('has Button text color set to "primaryButtonText" when "type" is not defined', () => {
    const style = wrapper.find('button span').prop('style');
    expect(style.color).toEqual(Theme.raisedButton.primary.labelColor);
  });

  it('has Button text font-size set to "14px"', () => {
    const style = wrapper.find('button span').prop('style');
    expect(style.fontSize).toEqual(14);
  });

  it('has Button text font-weight set to "500"(medium)', () => {
    const style = wrapper.find('button span').prop('style');
    expect(style.fontWeight).toEqual(500);
  });

  it('simulates click events', () => {
    wrapper.find('button').simulate('click');
    expect(onClick).toHaveBeenCalled();
  });
});

describe('SovosRaisedButton primary', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = renderRaisedButton({ type: 'primary' });
  });

  it('has Button backgroundColor set to "primaryButtonBackground" when "type" is "primary"', () => {
    const style = wrapper.find('button').prop('style');
    expect(style.backgroundColor).toEqual(Theme.raisedButton.primary.backgroundColor);
  });

  it('has Button text color set to "primaryButtonText" when "type" is "primary"', () => {
    const style = wrapper.find('button span').prop('style');
    expect(style.color).toEqual(Theme.raisedButton.primary.labelColor);
  });
});

describe('SovosRaisedButton secondary', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = renderRaisedButton({ type: 'secondary' });
  });

  it('has Button backgroundColor set to "secondaryButtonBackground" when "type" is "secondary"', () => {
    const style = wrapper.find('button').prop('style');
    expect(style.backgroundColor).toEqual(Theme.raisedButton.secondary.backgroundColor);
  });

  it('has Button text color set to "secondaryButtonText" when "type" is "secondary"', () => {
    const style = wrapper.find('button span').prop('style');
    expect(style.color).toEqual(Theme.raisedButton.secondary.labelColor);
  });
});

describe('SovosRaisedButton custom', () => {
  let wrapper;
  const backgroundColor = '#e53530';
  const labelColor = '#fedcdf';

  beforeEach(() => {
    wrapper = renderRaisedButton({ type: 'custom', backgroundColor, labelColor });
  });

  it('has Button backgroundColor set to "#e53530" when "type" is "custom" and labelColor prop is "#e53530"', () => {
    const style = wrapper.find('button').prop('style');
    expect(style.backgroundColor).toEqual(backgroundColor);
  });

  it('has Button text color set to "#fedcdf" when "type" is "custom" and backgroundColor prop is "#fedcdf"', () => {
    const style = wrapper.find('button span').prop('style');
    expect(style.color).toEqual(labelColor);
  });
});
