import React from 'react';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import SovosRaisedButton from './SovosRaisedButton';
import { yellowLight, yellowDark } from '../themes/sovos-colors';

export default () => (
  <Card style={ { textAlign: 'center', margin: '1rem' } }>
    <CardTitle title="Raised Buttons" />
    <CardText>
      <div>
        <SovosRaisedButton
          label="Primary"
          style={ { margin: '1rem' } }
        />
        <SovosRaisedButton
          label="Secondary"
          type="secondary"
          style={ { margin: '1rem' } }
        />
        <SovosRaisedButton
          label="Custom"
          type="custom"
          labelColor={ yellowLight }
          backgroundColor={ yellowDark }
          style={ { margin: '1rem' } }
        />
      </div>
      <div>
        <SovosRaisedButton
          disabled
          label="Primary"
          style={ { margin: '1rem' } }
        />
        <SovosRaisedButton
          disabled
          type="secondary"
          label="secondary"
          style={ { margin: '1rem' } }
        />
        <SovosRaisedButton
          disabled
          label="Custom"
          type="custom"
          labelColor={ yellowLight }
          backgroundColor={ yellowDark }
          style={ { margin: '1rem' } }
        />
      </div>
    </CardText>
  </Card>
);
