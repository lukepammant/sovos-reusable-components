import PropTypes from 'prop-types';
import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import RaisedButton from 'material-ui/RaisedButton';
import { fade } from 'material-ui/utils/colorManipulator';

const SovosRaisedButton = ({ type, muiTheme, ...otherProps }) => {
  let styles = {};
  
  switch (type) {
    default:
      console.warn(`Unrecognized type ${type}. Defaulting to primary.`);
      type = 'primary';
    case 'primary':
    case 'secondary':
      styles = muiTheme.raisedButton[type];
      break;
    case 'custom':
      const { labelColor, backgroundColor } = otherProps;

      styles = {
        labelColor,
        backgroundColor,
        disabledBackgroundColor: fade(backgroundColor, 0.6),
        disabledLabelColor: fade(labelColor, 0.6)
      };
      break;
  }
  
  return (
    <RaisedButton
      { ...otherProps }
      { ...styles }
    />);
};

SovosRaisedButton.propTypes = Object.assign({},
  RaisedButton.propTypes,
  {
    type: PropTypes.oneOf(['primary', 'secondary', 'custom']),
    color: PropTypes.string
  });

SovosRaisedButton.defaultProps = {
  type: 'primary'
};

export default muiThemeable()(SovosRaisedButton);
