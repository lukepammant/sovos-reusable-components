import React from 'react';
import { DatePicker } from 'material-ui';

const SovosDatePicker = function (props) {
  props = Object.assign({}, props, { container: 'inline' });
  return <DatePicker { ...props } />;
};

SovosDatePicker.propTypes = DatePicker.propTypes;

export default SovosDatePicker;
