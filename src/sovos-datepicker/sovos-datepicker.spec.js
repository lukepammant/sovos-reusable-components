import React from 'react';
import { shallow } from 'enzyme';
import SovosDatePicker from './SovosDatePicker';

describe('SovosDatePicker', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(
      <SovosDatePicker />
    );
  });

  it('has DatePicker container set to inline', () => {
    const container = wrapper.find('DatePicker').prop('container');
    expect(container).toEqual('inline');
  });
});
