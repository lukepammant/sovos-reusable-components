import React from 'react';
import PropTypes from 'prop-types';
import muiThemeable from 'material-ui/styles/muiThemeable';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';
import MenuItem from 'material-ui/MenuItem';
import Popover from 'material-ui/Popover';
import DropDownArrow from 'material-ui/svg-icons/navigation/arrow-drop-down';
import { Tabs, Tab } from 'material-ui/Tabs';
import SovosIconButton from '../sovos-icon-button/SovosIconButton';
import SovosFlatButton from '../sovos-flat-button/SovosFlatButton';

class SovosTitleBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      popoverOpen: false,
    };
  }

  getStyle = element =>
    this.props.muiTheme.globalLayout.titleBar[element];

  shouldShowTabs = () =>
    this.props.tabs && this.props.tabs.length > 0;

  closeBreadCrumbDropDown = () =>
    this.setState({ popoverOpen: false });

  expandBreadCrumbDropDown = (event) => {
    event.preventDefault();

    this.setState({
      popoverOpen: true,
      anchorEl: event.currentTarget,
    });
  };

  renderClickableBreadCrumb = item => (
    <SovosFlatButton
      className="sovos-TitleBar__TopSection__bread-crumb-clickable"
      label={ item.text }
      labelPosition="before"
      style={ { height: 50, minWidth: 0 } }
      labelStyle={ this.getStyle('breadCrumbClickable') }
      type="custom"
      color="#000"
      onClick={ item.onClick || this.expandBreadCrumbDropDown }
      icon={ item.nestedItems && <DropDownArrow /> }
    />
  )

  renderNonClickableBreadCrumb = item => (
    <span
      className="sovos-TitleBar__TopSection__bread-crumb-non-clickable"
      style={ this.getStyle('breadCrumbNonClickable') }
    >
      {item.text}
    </span>
  )

  renderDropdownItem = (element, index) => (
    <MenuItem
      key={ `NestedBreadCrumbItem${index}-${element.text}` }
      primaryText={ element.text }
      onClick={ element.action }
    />
  );

  renderBreadCrumbDropdown = element => (
    <Popover
      className="sovos-TitleBar__TopSection__bread-crumb-dropdown"
      open={ this.state.popoverOpen }
      anchorEl={ this.state.anchorEl }
      anchorOrigin={ { horizontal: 'left', vertical: 'bottom' } }
      targetOrigin={ { horizontal: 'left', vertical: 'top' } }
      onRequestClose={ this.closeBreadCrumbDropDown }
    >
      {element.nestedItems && element.nestedItems.length > 0 &&
        element.nestedItems.map(this.renderDropdownItem)
      }
    </Popover>
  )

  renderBreadCrumb = (element, index) => {
    const { titleElements } = this.props;
    const isClickable = !!element.onClick || !!element.nestedItems;
    const isLastItem = index === titleElements.length - 1;

    const opacity = isLastItem || !isClickable
      ? { opacity: 0.9 }
      : { opacity: 0.6 };

    return (
      <div
        key={ `BreadCrumb${index}-${element.text}` }
        className="sovos-TitleBar__TopSection__bread-crumb"
        style={ { ...this.getStyle('breadCrumb'), ...opacity } }
      >
        {isClickable
          ? this.renderClickableBreadCrumb(element)
          : this.renderNonClickableBreadCrumb(element)
        }

        {element.nestedItems &&
          this.renderBreadCrumbDropdown(element)
        }

        {!isLastItem && <span>/</span>}
      </div>);
  }

  renderBreadCrumbsSection = () => {
    const { navigationCollapsed, titleElements } = this.props;
    const style = {
      padding: navigationCollapsed ? '' : '0 20px',
      ...this.getStyle('breadCrumbContainer')
    };

    return (
      <div
        className="sovos-TitleBar__TopSection__bread-crumbs-container"
        style={ style }
      >
        {titleElements.map(this.renderBreadCrumb)}
      </div>);
  }

  renderTab = (tab, index) => {
    const isSelected = typeof tab.selected === 'function' ? tab.selected() : tab.selected;
    const style = { color: isSelected ? 'rgb(64,192,240)' : 'rgba(0,0,0,.6)' };

    return (
      <Tab
        key={ `Tab${index}-${tab.label}` }
        label={ tab.label }
        onClick={ tab.onClick }
        style={ style }
      />
    );
  }

  renderTabSection = () => {
    const { tabs, browser } = this.props;

    if (!this.shouldShowTabs()) return null;

    let width = 100 * tabs.length;
    width = Math.min(width, browser.width);

    return (
      <Tabs
        className="sovos-TitleBar__TabsSection"
        onChange={ this.handleChange }
        value={ this.state.slideIndex }
        style={ this.getStyle('tabsContainer') }
        tabItemContainerStyle={ { width, ...this.getStyle('tabItemContainer') } }
        inkBarStyle={ this.getStyle('tabsInkBar') }
      >
        {tabs.map(this.renderTab)}
      </Tabs>
    );
  }

  renderNavigationToggleButton = () => (
    this.props.navigationCollapsed &&
    <SovosIconButton
      className="sovos-TitleBar__TopSection__navigation-button"
      style={ this.getStyle('navigationMenuButton') }
      onClick={ this.props.toggleNavigation }
    >
      <MenuIcon />
    </SovosIconButton>
  );

  renderRightIcons = () => (
    this.props.rightIcons && this.props.rightIcons.length > 0 &&
    <div
      className="sovos-TitleBar__TopSection__right-icons"
      style={ this.getStyle('rightIconContainer') }
    >
      {this.props.rightIcons}
    </div>
  )

  render() {
    const style = {
      ...this.getStyle('container'),
      height: this.shouldShowTabs() ? 100 : 50,
    };

    return (
      <div className="sovos-TitleBar" style={ style }>
        <div className="sovos-TitleBar__TopSection" style={ this.getStyle('topSection') }>
          {this.renderNavigationToggleButton()}
          {this.renderBreadCrumbsSection()}
          {this.renderRightIcons()}
        </div>

        {this.renderTabSection()}
      </div>
    );
  }
}

const titleElementProps = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func
};

const tabElementProps = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
};

SovosTitleBar.propTypes = {
  muiTheme: PropTypes.object.isRequired,
  browser: PropTypes.object.isRequired,
  navigationCollapsed: PropTypes.bool.isRequired,
  rightIcons: PropTypes.node,
  tabs: PropTypes.arrayOf(
    PropTypes.shape(tabElementProps)
  ),
  titleElements: PropTypes.arrayOf(
    PropTypes.shape(titleElementProps)
  ).isRequired,
  toggleNavigation: PropTypes.func.isRequired,
};

export const TitleElementProps = titleElementProps;
export const TabElementProps = tabElementProps;
export default muiThemeable()(SovosTitleBar);
