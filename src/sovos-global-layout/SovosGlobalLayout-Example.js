import React from 'react';
import { render } from 'react-dom';
import ActionSearch from 'material-ui/svg-icons/action/search';
import ActionSettings from 'material-ui/svg-icons/action/settings';
import ActionSystemUpdateAlt from 'material-ui/svg-icons/action/system-update-alt';
import FileFolder from 'material-ui/svg-icons/file/folder';
import SocialGroup from 'material-ui/svg-icons/social/group';
import Toggle from 'material-ui/Toggle';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import SovosThemeProvider from '../sovos-theme-provider/SovosThemeProvider';
import SovosGlobalLayout from '../sovos-global-layout/SovosGlobalLayout';
import SovosIconButton from '../sovos-icon-button/SovosIconButton';
import SovosRaisedButton from '../sovos-raised-button/SovosRaisedButton';
import SovosFlatButton from '../sovos-flat-button/SovosFlatButton';

const contexts = [
  {
    name: 'PROD-UNIT1',
    type: 'production',
    taxYear: '2016'
  },
  {
    name: 'TEST-UNIT1',
    type: 'test',
    taxYear: '2018'
  },
  {
    name: 'DEFAULT-UNIT1',
    type: 'default',
    taxYear: '2013'
  }
];

const footerElements = [
  <SovosIconButton size="large" color="#CCC">
    <ActionSearch />
  </SovosIconButton>,
  <SovosIconButton size="large" color="#CCC">
    <FileFolder />
  </SovosIconButton>
];

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      showHero: true,
      breadCrumbHasNestedItems: true,
      titleBarActionButtons: true,
      content: 'Search',
      pageDepth: 2,
      selectedTab: 1,
      showTabs: true,
    };
    this.addATab();
  }

  getNestedBreadCrumbs = () => (
    this.state.breadCrumbHasNestedItems ? [
      {
        text: 'Sub Page 1',
        onClick: () => this.setState({ content: 'Sub Page 1', pageDepth: 2 }),
      }, {
        text: 'Sub Page 2',
        onClick: () => this.setState({ content: 'Sub Page 2', pageDepth: 2 }),
      }, {
        text: 'Sub Page 3',
        onClick: () => this.setState({ content: 'Sub Page 3', pageDepth: 2 }),
      }, {
        text: 'Sub Page 4',
        onClick: () => this.setState({ content: 'Sub Page 4', pageDepth: 2 }),
      }] : null
  )

  getBreadCrumbs = () =>
    [{
      text: 'Root Page',
      nestedItems: this.state.pageDepth === 0 && this.getNestedBreadCrumbs(),
      onClick: this.state.pageDepth > 0 && (
        () => this.setState({ content: 'Root Page', pageDepth: 0 })),
    },
    {
      text: 'Parent Page',
      nestedItems: this.state.pageDepth === 1 && this.getNestedBreadCrumbs(),
      onClick: this.state.pageDepth > 1 && (
        () => this.setState({ content: 'Parent Page', pageDepth: 1 })),
    },
    {
      text: 'Current Page',
      nestedItems: this.getNestedBreadCrumbs(),
    }].filter((v, i) => i <= this.state.pageDepth);

  getNavProps = () => ({
    contexts,
    selectedContext: contexts[0],
    setContext: ctx => this.setState({ content: `${ctx.name} Page` }),
    branding: { productName: '1099 ENTERPRISE' },
    items: [
      { label: 'Search', action: () => this.setState({ content: 'Search' }), icon: ActionSearch },
      { label: 'Import', action: () => this.setState({ content: 'Import' }), icon: ActionSystemUpdateAlt },
      { label: 'Recipients', action: () => this.setState({ content: 'Recipients' }), icon: SocialGroup },
      {
        label: 'Tools and Settings', icon: ActionSettings, nestedItems: [
          { label: 'Manage Files', action: () => this.setState({ content: 'Manage Files' }) },
          { label: 'Manage Settings', action: () => this.setState({ content: 'Manage Settings' }) }
        ]
      },
    ],
    selectedLink: 'Search',
    footerElements,
  });

  removeATab = () => {
    const tabs = this.state.tabs.slice(0, this.state.tabs.length - 1);
    this.setState({ tabs });
  }

  addATab = () => {
    const tabNumber = this.state.tabs ? this.state.tabs.length + 1 : 1;
    const tabs = this.state.tabs || [];
    tabs.push({
      label: `tab ${tabNumber}`,
      onClick: () => this.setState({ content: `tab ${tabNumber}`, selectedTab: tabNumber }),
      selected: () => this.state.selectedTab === tabNumber
    });
    this.setState({ tabs });
  }

  getAppBarActions = () => [
    (<SovosRaisedButton label="Action" />),
    (<IconMenu
      iconButtonElement={ <IconButton><MoreVertIcon /></IconButton> }
      targetOrigin={ { horizontal: 'right', vertical: 'top' } }
      anchorOrigin={ { horizontal: 'right', vertical: 'top' } }
    >
      <MenuItem primaryText="Refresh" />
      <MenuItem primaryText="Help" />
      <MenuItem primaryText="Sign out" />
    </IconMenu>)
  ]

  getHeroBannerProps = () => ({
    title: `${this.state.content} banner`,
    subtitle: 'Subtitle test',
    subtitle2: 'Subtitle test 2',
  })

  render() {
    return (
      <SovosThemeProvider>
        <SovosGlobalLayout
          titleElements={ this.getBreadCrumbs() }
          contexts={ contexts }
          selectedContext={ contexts[0] }
          navigationProps={ this.getNavProps() }
          titleBarActions={ this.state.titleBarActionButtons && this.getAppBarActions() }
          heroBanner={ this.state.showHero && this.getHeroBannerProps() }
          titleBarTabs={ this.state.showTabs && this.state.tabs }
        >
          <Toggle
            style={ { marginTop: 10 } }
            label="Toggle Hero Banner"
            labelPosition="right"
            defaultToggled
            onToggle={ (_, toggled) => this.setState({ showHero: toggled }) }
          />
          <Toggle
            style={ { marginTop: 10 } }
            label="Toggle Bread Crumb Nested Items"
            labelPosition="right"
            defaultToggled
            onToggle={ (_, toggled) => this.setState({ breadCrumbHasNestedItems: toggled }) }
          />
          <Toggle
            style={ { marginTop: 10 } }
            label="Toggle Title Bar Action Buttons"
            labelPosition="right"
            defaultToggled={ this.state.titleBarActionButtons }
            onToggle={ (_, toggled) => this.setState({ titleBarActionButtons: toggled }) }
          />
          <Toggle
            style={ { marginTop: 10 } }
            label="Toggle Tabs"
            labelPosition="right"
            defaultToggled={ this.state.showTabs }
            onToggle={ (_, toggled) => this.setState({ showTabs: toggled }) }
          />
          <SovosFlatButton label="Remove a Tab" onClick={ this.removeATab } />
          <SovosFlatButton label="Add a Tab" onClick={ this.addATab } />
          <p>{this.state.content} content</p>
        </SovosGlobalLayout>
      </SovosThemeProvider>
    );
  }
}

render(<App />, document.getElementById('root'));
