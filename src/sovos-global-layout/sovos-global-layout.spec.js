import React from 'react';
import { mount } from 'enzyme';
import ActionSearch from 'material-ui/svg-icons/action/search';
import SovosGlobalLayout from './SovosGlobalLayout';
import SovosThemeProvider from '../sovos-theme-provider/SovosThemeProvider';

const doNothing = () => {};

const defaultNavProps = {
  contexts: [{ name: 'test', type: 'production', taxYear: '2018' }],
  selectedContext: { name: 'test', type: 'production', taxYear: '2018' },
  branding: { productName: 'test' },
  items: [{ label: 'test', action: () => { }, icon: ActionSearch }],
  initialSelectedLink: 'test',
  footerElements: [<ActionSearch key="abc" />],
  setContext: doNothing,
};

const renderGlobalLayout = (props) => {
  props = props || {};
  return mount(<SovosThemeProvider>
    <SovosGlobalLayout
      breakPoint={ props.breakPoint }
      browser={ props.browser || { width: 1000 } }
      initialNavigationCollapsed={ props.initialNavigationCollapsed }
      toggleNavigation={ () => { } }
      contexts={ defaultNavProps.contexts }
      titleElements={ props.titleElements || [{ text: 'test' }] }
      navigationProps={ defaultNavProps }
      titleBarActions={ props.titleBarActions || [] }
      heroBanner={ props.heroBanner }
      titleBarTabs={ props.titleBarTabs || [] }
    />
  </SovosThemeProvider>);
};

describe('SovosGlobalLayout', () => {
  describe('Navigation', () => {
    it('doesn\'t show if initialNavigationCollapsed set', () => {
      const wrapper = renderGlobalLayout({ initialNavigationCollapsed: true });
      const navigationStyles = wrapper.find('.sovos-navigation').first().prop('style');
      expect(navigationStyles.width).toEqual(0);
    });

    it('shows if initialNavigationCollapsed not set', () => {
      const wrapper = renderGlobalLayout();
      const navigationStyles = wrapper.find('.sovos-navigation').first().prop('style');
      expect(navigationStyles.width).toEqual(240);
    });

    it('doesn\'t show if browser width is less than breakpoint', () => {
      const wrapper = renderGlobalLayout({ browser: { width: 100 }, breakPoint: 150 });
      const navigationStyles = wrapper.find('.sovos-navigation').first().prop('style');
      expect(navigationStyles.width).toEqual(0);
    });

    it('shows if browser width is more than breakpoint', () => {
      const wrapper = renderGlobalLayout({ browser: { width: 150 }, breakPoint: 100 });
      const navigationStyles = wrapper.find('.sovos-navigation').first().prop('style');
      expect(navigationStyles.width).toEqual(240);
    });

    it('clickable overlay hides navigation on click if browser width is less than breakpoint', () => {
      const wrapper = renderGlobalLayout({ browser: { width: 100 }, breakPoint: 150, initialNavigationCollapsed: false });
      const initialWidth = wrapper.find('.sovos-navigation').first().prop('style').width;

      wrapper.find('.sovos-global-layout__overlay').first().simulate('click');

      const widthAfterOverlayClick = wrapper.find('.sovos-navigation').first().prop('style').width;
      expect(initialWidth).toEqual(240);
      expect(widthAfterOverlayClick).toEqual(0);
    });
  });
});
