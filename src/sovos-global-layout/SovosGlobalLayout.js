import React from 'react';
import PropTypes from 'prop-types';
import muiThemeable from 'material-ui/styles/muiThemeable';
import Paper from 'material-ui/Paper';
import SovosNavigation, { sovosNavigationProps } from '../sovos-navigation/SovosNavigation';
import SovosHeroBanner from '../sovos-hero-banner/SovosHeroBanner';
import SovosTitleBar, { TitleElementProps } from './SovosTitleBar';

class SovosGlobalLayout extends React.Component {
  constructor(props) {
    super(props);
    const { initialNavigationCollapsed } = this.props;
    this.state = {
      navigationCollapsed: typeof initialNavigationCollapsed !== 'undefined'
        ? initialNavigationCollapsed
        : this.isSmallerThanBreakpoint(),
    };

    window.addEventListener('resize', () => {
      const isSmallerThanBreakpoint = this.isSmallerThanBreakpoint();
      if (isSmallerThanBreakpoint !== this.state.navigationCollapsed) {
        this.setState({ navigationCollapsed: isSmallerThanBreakpoint });
      }
    });
  }

  onLinkClick = (action) => {
    action();
    this.toggleNavigation(false);
  }

  toggleNavigation = (collapsed) => {
    this.setState({
      navigationCollapsed: typeof collapsed === 'boolean'
        ? !collapsed
        : !this.state.navigationCollapsed
    });
  }

  isSmallerThanBreakpoint = () =>
    this.props.browser.width < this.props.breakPoint;

  renderTitleBarAndPage = () => {
    const { navigationCollapsed } = this.state;
    const { muiTheme, wrapContentInPaper, children, style, heroBanner, browser } = this.props;
    const styles = muiTheme.globalLayout;

    return (
      <div
        style={ {
          left: navigationCollapsed ? 0 : 240,
          ...styles.appBarAndContentContainer,
          ...style
        } }
      >
        <SovosTitleBar
          browser={ browser }
          titleElements={ this.props.titleElements }
          navigationCollapsed={ this.isSmallerThanBreakpoint() }
          toggleNavigation={ this.toggleNavigation }
          rightIcons={ this.props.titleBarActions }
          tabs={ this.props.titleBarTabs }
        />
        <div style={ { padding: 20 } }>
          {
            heroBanner &&
            <SovosHeroBanner
              parentPadding={ { top: 20, left: 20, right: 20 } }
              { ...heroBanner }
            />
          }
          {wrapContentInPaper ?
            <Paper style={ { padding: 20, marginTop: heroBanner ? 20 : 0 } }>
              {children}
            </Paper>
            : children}
        </div>
      </div>);
  }

  renderNavigation = () => (
    <SovosNavigation
      { ...this.props.navigationProps }
      branding={ this.props.navigationProps.branding }
      footerElements={ this.props.navigationProps.footerElements }
      links={ this.props.navigationProps.links }
      initialSelectedLink={ this.props.navigationProps.initialSelectedLink }
      collapsed={ this.state.navigationCollapsed }
      onItemClick={ this.onLinkClick }
      onCollapseToggled={ this.toggleNavigation }
      hideDrawer={ () => this.isSmallerThanBreakpoint() && this.toggleNavigation(false) }
    />
  );

  renderOverlay = () => (
    <div
      className="sovos-global-layout__overlay"
      onClick={ () => this.toggleNavigation(false) }
      style={
        this.isSmallerThanBreakpoint() && !this.state.navigationCollapsed
          ? this.props.muiTheme.globalLayout.overlayVisible
          : this.props.muiTheme.globalLayout.overlayHidden
      }
    />
  );

  render() {
    return (
      <div>
        {this.renderNavigation()}
        {this.renderOverlay()}
        {this.renderTitleBarAndPage()}
      </div>
    );
  }
}

SovosGlobalLayout.propTypes = {
  breakPoint: PropTypes.number,
  browser: PropTypes.object.isRequired,
  children: PropTypes.node,
  initialNavigationCollapsed: PropTypes.bool,
  heroBanner: PropTypes.object,
  muiTheme: PropTypes.object.isRequired,
  navigationProps: PropTypes.shape({
    branding: PropTypes.shape(sovosNavigationProps.branding),
    footerElements: sovosNavigationProps.footerElements,
    links: sovosNavigationProps.links,
    initialSelectedLink: sovosNavigationProps.initialSelectedLink,
  }),
  style: PropTypes.object,
  titleBarTabs: PropTypes.arrayOf(PropTypes.object),
  titleBarActions: PropTypes.node,
  titleElements: PropTypes.arrayOf(
    PropTypes.shape(TitleElementProps)
  ).isRequired,
  wrapContentInPaper: PropTypes.bool,
};

SovosGlobalLayout.defaultProps = {
  breakPoint: 960,
  wrapContentInPaper: true,
};

export default muiThemeable()(SovosGlobalLayout);
