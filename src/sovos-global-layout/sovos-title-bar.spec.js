import React from 'react';
import { mount } from 'enzyme';
import ActionSearch from 'material-ui/svg-icons/action/search';
import SovosTitleBar from './SovosTitleBar';
import SovosThemeProvider from '../sovos-theme-provider/SovosThemeProvider';

const onClick = () => { };

const renderTitleBar = (props) => {
  props = props || {};
  return mount(
    <SovosThemeProvider>
      <SovosTitleBar
        navigationCollapsed={ props.navigationCollapsed || false }
        rightIcons={ props.rightIcons || [] }
        tabs={ props.tabs || [] }
        titleElements={ props.titleElements || [{ text: 'test' }] }
        toggleNavigation={ () => { } }
        browser={ props.browser || { width: 1000 } }
      />
    </SovosThemeProvider>);
};

describe('SovosTitleBar', () => {
  describe('Navigation Toggle', () => {
    it('shows if navigation is collapsed', () => {
      const wrapper = renderTitleBar({ navigationCollapsed: true });
      const tabsSection = wrapper.find('.sovos-TitleBar__TopSection__navigation-button');
      expect(tabsSection.exists()).toEqual(true);
    });

    it('hides if navigation is expanded', () => {
      const wrapper = renderTitleBar({ navigationCollapsed: false });
      const tabsSection = wrapper.find('.sovos-TitleBar__TopSection__navigation-button');
      expect(tabsSection.exists()).toEqual(false);
    });
  });

  describe('Right Action Items', () => {
    it('shows if right action buttons are defined', () => {
      const rightIcons = [(<ActionSearch key="abc123" />)];
      const wrapper = renderTitleBar({ rightIcons });
      const rightIconsSection = wrapper.find('.sovos-TitleBar__TopSection__right-icons');
      expect(rightIconsSection.exists()).toEqual(true);
    });

    it('hides if right action buttons are not defined', () => {
      const wrapper = renderTitleBar();
      const rightIconsSection = wrapper.find('.sovos-TitleBar__TopSection__right-icons');
      expect(rightIconsSection.exists()).toEqual(false);
    });
  });

  describe('Bread Crumbs', () => {
    it('shows non-clickable breadcrumb if no onClick provided', () => {
      const wrapper = renderTitleBar({ titleElements: [{ text: 'test' }] });
      const nonClickableBreadCrumb = wrapper.find('.sovos-TitleBar__TopSection__bread-crumb-non-clickable');
      const clickableBreadCrumb = wrapper.find('.sovos-TitleBar__TopSection__bread-crumb-clickable');
      expect(clickableBreadCrumb.exists()).toEqual(false);
      expect(nonClickableBreadCrumb.exists()).toEqual(true);
    });

    it('shows clickable breadcrumb if an onClick method is provided', () => {
      const wrapper = renderTitleBar({ titleElements: [{ text: 'test', onClick }] });
      const clickableBreadCrumb = wrapper.find('.sovos-TitleBar__TopSection__bread-crumb-clickable');
      const nonClickableBreadCrumb = wrapper.find('.sovos-TitleBar__TopSection__bread-crumb-non-clickable');
      expect(clickableBreadCrumb.exists()).toEqual(true);
      expect(nonClickableBreadCrumb.exists()).toEqual(false);
    });

    it('shows dropdown menu if item has nested items', () => {
      const item = [{ text: 'test', nestedItems: [{ text: 'nested item' }] }];
      const wrapper = renderTitleBar({ titleElements: item });
      const dropDownButton = wrapper.find('svg');
      expect(dropDownButton.exists()).toEqual(true);
    });

    it('items without nested items don\'t have a dropdown menu', () => {
      const wrapper = renderTitleBar({ titleElements: [{ text: 'test', onClick }] });
      const dropDownButton = wrapper.find('svg');
      expect(dropDownButton.exists()).toEqual(false);
    });
  });

  describe('Tabs', () => {
    it('shows tabs section if tabs defined in props', () => {
      const tabs = [{ label: 'tab 1', onClick }];
      const wrapper = renderTitleBar({ tabs });
      const tabsSection = wrapper.find('.sovos-TitleBar__TabsSection');
      expect(tabsSection.exists()).toEqual(true);
    });

    it('doesn\'t show tabs section if no tabs in props', () => {
      const wrapper = renderTitleBar();
      const tabsSection = wrapper.find('.sovos-TitleBar__TabsSection');
      expect(tabsSection.exists()).toEqual(false);
    });

    it('has a container width of 200px if two tabs are defined', () => {
      const tabs = [{ label: 'tab 1', onClick }, { label: 'tab 2', onClick }];
      const wrapper = renderTitleBar({ tabs });
      const style = wrapper.find('.sovos-TitleBar__TabsSection > div').first().prop('style');
      expect(style.width).toEqual(200);
    });

    it('has a container width set to the same as the browser if tab width exceeds browser width', () => {
      const browser = { width: 300 };
      const tab = { label: 'tab 1', onClick };
      const tabs = [tab, tab, tab, tab, tab, tab, tab, tab];
      const wrapper = renderTitleBar({ tabs, browser });
      const style = wrapper.find('.sovos-TitleBar__TabsSection > div').first().prop('style');
      expect(style.width).toEqual(browser.width);
    });
  });
});
