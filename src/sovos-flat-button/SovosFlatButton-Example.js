import React from 'react';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import SovosFlatButton from './SovosFlatButton';
import { yellowDark } from '../themes/sovos-colors';

export default () => (
  <Card style={ { textAlign: 'center', margin: '1rem' } }>
    <CardTitle title="Flat Buttons" />
    <CardText>
      <div>
        <SovosFlatButton
          label="Primary"
          type="primary"
          style={ { margin: '1rem' } }
        />
        <SovosFlatButton
          label="Secondary"
          type="secondary"
          style={ { margin: '1rem' } }
        />
        <SovosFlatButton
          label="Custom"
          type="custom"
          color={ yellowDark }
          style={ { margin: '1rem' } }
        />
      </div>
      <div>
        <SovosFlatButton
          disabled
          label="Primary"
          type="primary"
          style={ { margin: '1rem' } }
        />
        <SovosFlatButton
          disabled
          label="Secondary"
          type="secondary"
          style={ { margin: '1rem' } }
        />
        <SovosFlatButton
          disabled
          label="Custom"
          type="custom"
          color={ yellowDark }
          style={ { margin: '1rem' } }
        />
      </div>
    </CardText>
  </Card>
);
