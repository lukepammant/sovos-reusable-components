import PropTypes from 'prop-types';
import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import FlatButton from 'material-ui/FlatButton';
import { fade } from 'material-ui/utils/colorManipulator';

const SovosFlatButton = ({ type, disabled, color, muiTheme, ...otherProps }) => {
  let textColor;
  let hoverColor;

  switch (type.toLowerCase()) {
    case 'custom':
      textColor = disabled ? fade(color, 0.3) : color;
      hoverColor = fade(color, 0.15);
      break;
    default:
      console.warn(`Unrecognized type ${type}. Defaulting to primary.`);
      type = 'primary';
    case 'primary':
    case 'secondary':
      const style = { ...muiTheme.flatButton[type] };
      textColor = disabled ? fade(style.labelColor, 0.3) : style.labelColor;
      hoverColor = style.hoverColor;
      break;
  }

  const labelStyle = {
    ...muiTheme.flatButton.labelStyle,
    ...otherProps.labelStyle,
    color: textColor
  };

  return (
    <FlatButton
      { ...otherProps }
      disabled={ disabled }
      hoverColor={ hoverColor }
      labelStyle={ labelStyle }
    />);
};

SovosFlatButton.propTypes = Object.assign({},
  FlatButton.propTypes,
  {
    type: PropTypes.oneOf(['primary', 'secondary', 'custom']),
    color: PropTypes.string
  });

SovosFlatButton.defaultProps = {
  type: 'primary'
};

export default muiThemeable()(SovosFlatButton);
