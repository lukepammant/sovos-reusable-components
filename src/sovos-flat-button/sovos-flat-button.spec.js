import React from 'react';
import { mount } from 'enzyme';
import SovosFlatButton from './SovosFlatButton';
import SovosThemeProvider from '../sovos-theme-provider/SovosThemeProvider';
import Theme from '../themes/sovos-enterprise';

const renderFlatButton = ({ type, color, onClick }) =>
  mount(
    <SovosThemeProvider>
      <SovosFlatButton
        label="Testing"
        color={ color }
        type={ type }
        onClick={ onClick }
      />
    </SovosThemeProvider>
  );

describe('SovosFlatButton default', () => {
  let wrapper;
  let onClick;

  beforeEach(() => {
    onClick = sinon.spy();
    wrapper = renderFlatButton({ onClick });
  });

  it('has Button text color set to "primaryFlatButtonText" when "type" is not defined', () => {
    const style = wrapper.find('button span').prop('style');
    expect(style.color).toEqual(Theme.flatButton.primary.labelColor);
  });

  it('has Button text font-size set to "14px"', () => {
    const style = wrapper.find('button span').prop('style');
    expect(style.fontSize).toEqual('14px');
  });

  it('has Button text font-weight set to "500"(medium)', () => {
    const style = wrapper.find('button span').prop('style');
    expect(style.fontWeight).toEqual('500');
  });

  it('simulates click events', () => {
    wrapper.find('button').simulate('click');
    expect(onClick).toHaveBeenCalled();
  });
});

describe('SovosFlatButton primary', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = renderFlatButton({ type: 'primary' });
  });

  it('has Button text color set to "primaryButtonText" when "type" is "primary"', () => {
    const style = wrapper.find('button span').prop('style');
    expect(style.color).toEqual(Theme.flatButton.primary.labelColor);
  });
});

describe('SovosFlatButton secondary', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = renderFlatButton({ type: 'secondary' });
  });

  it('has Button text color set to "secondaryButtonText" when "type" is "secondary"', () => {
    const style = wrapper.find('button span').prop('style');
    expect(style.color).toEqual(Theme.flatButton.secondary.labelColor);
  });
});

describe('SovosFlatButton custom', () => {
  let wrapper;
  const color = '#fedcdf';

  beforeEach(() => {
    wrapper = renderFlatButton({ type: 'custom', color });
  });

  it('has Button text color set to "#fedcdf" when "type" is "custom" and color is "#fedcdf"', () => {
    const style = wrapper.find('button span').prop('style');
    expect(style.color).toEqual(color);
  });
});
