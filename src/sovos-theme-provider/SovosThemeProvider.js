import PropTypes from 'prop-types';
import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import injectTapEventPlugin from 'react-tap-event-plugin';
import sovosEnterpriseTheme from '../themes/sovos-enterprise';

injectTapEventPlugin();

class SovosThemeProvider extends Component {
  componentDidMount() {
    // Add our custom fonts
    const typographyCss = document.createElement('link');
    typographyCss.rel = 'stylesheet';
    typographyCss.type = 'text/css';
    typographyCss.href = '//cloud.typography.com/6015574/787028/css/fonts.css';
    document.head.appendChild(typographyCss);

    // Set defaults
    document.body.style.fontFamily = this.props.theme.fontFamily || sovosEnterpriseTheme.fontFamily;
    document.body.style.margin = 0;
    document.body.style.padding = 0;
  }

  render() {
    return (
      <MuiThemeProvider muiTheme={ getMuiTheme(this.props.theme) }>
        { this.props.children }
      </MuiThemeProvider>
    );
  }
}

SovosThemeProvider.defaultProps = {
  theme: sovosEnterpriseTheme
};

SovosThemeProvider.propTypes = {
  children: PropTypes.node.isRequired,
  /* eslint-disable */
  theme: PropTypes.object
  /* eslint-enable */
};

export default SovosThemeProvider;
