import PropTypes from 'prop-types';
import React from 'react';
import { MenuItem, SelectField } from 'material-ui';

require('./pagination-footer.scss');

const color = 'rgba(0,0,0,.54)';
const fontSize = 12;

class PaginationFooter extends React.Component {
  shouldComponentUpdate(nextProps) {
    return (
      nextProps.endItem !== this.props.endItem ||
      nextProps.itemsPerPage !== this.props.itemsPerPage ||
      nextProps.startItem !== this.props.startItem ||
      nextProps.totalItems !== this.props.totalItems
    );
  }

  renderItems = () => (
    this.props.itemsPerPageOptions.map(value => (
      <MenuItem
        key={ value }
        primaryText={ value }
        style={ { color, fontSize } }
        value={ value }
      />
    ))
  );

  render() {
    return (
      <div className="sovos-PaginationFooter">
        <div className="sovos-PaginationFooter__Element">Items per page: </div>
        <SelectField
          iconStyle={ { color, fill: color, height: 24, width: 24, top: 3 } }
          labelStyle={ { color, marginRight: 0, paddingRight: 0, textAlign: 'right', top: 0, width: 40 } }
          onChange={ this.props.onPerPageChanged }
          style={ { color, fontSize, width: 64, top: 24 } }
          underlineStyle={ { borderBottom: 'none' } }
          value={ this.props.itemsPerPage }
        >
          { this.renderItems() }
        </SelectField>
        <div className="sovos-PaginationFooter__Element sovos-PaginationFooter__Element_counts">
          { this.props.startItem } - { this.props.endItem } of { this.props.totalItems }
        </div>
        <button
          className="sovos-PaginationFooter__Prev sovos-PaginationFooter__Arrow"
          onClick={ this.props.onPrevClicked }
        >
          &#10094;
        </button>
        <button
          className="sovos-PaginationFooter__Next sovos-PaginationFooter__Arrow"
          onClick={ this.props.onNextClicked }
        >
          &#10095;
        </button>
      </div>
    );
  }
}

PaginationFooter.displayName = 'PaginationFooter';

PaginationFooter.propTypes = {
  endItem: PropTypes.number.isRequired,
  itemsPerPage: PropTypes.number.isRequired,
  onNextClicked: PropTypes.func.isRequired,
  onPerPageChanged: PropTypes.func.isRequired,
  onPrevClicked: PropTypes.func.isRequired,
  itemsPerPageOptions: PropTypes.array,
  startItem: PropTypes.number.isRequired,
  totalItems: PropTypes.number.isRequired
};

PaginationFooter.defaultProps = {
  itemsPerPageOptions: [50, 100, 200, 300]
};

export default PaginationFooter;
