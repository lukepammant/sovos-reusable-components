import React from 'react';
import { shallow } from 'enzyme';
import PaginationFooter from './PaginationFooter';

describe('PaginationFooter', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(
      <PaginationFooter
        endItem={ 50 }
        itemsPerPage={ 50 }
        onNextClicked={ sinon.spy() }
        onPerPageChanged={ sinon.spy() }
        onPrevClicked={ sinon.spy() }
        startItem={ 1 }
        totalItems={ 100 }
      />
    );
  });

  it('calls onPrevClicked when prev button is clicked', () => {
    wrapper.find('.sovos-PaginationFooter__Prev').simulate('click');
    expect(wrapper.instance().props.onPrevClicked).toHaveBeenCalled();
  });

  it('calls onNextClicked when next button is clicked', () => {
    wrapper.find('.sovos-PaginationFooter__Next').simulate('click');
    expect(wrapper.instance().props.onNextClicked).toHaveBeenCalled();
  });

  it('calls onPerPageChanged when items per page is changed', () => {
    wrapper.find('SelectField').prop('onChange')();
    expect(wrapper.instance().props.onPerPageChanged).toHaveBeenCalled();
  });
});
