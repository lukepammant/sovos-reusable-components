import React from 'react';
import { TableRow } from 'material-ui';

const SovosSelectableAndClickableTableRow = function (props) {
  const originalOnRowClick = props.onRowClick;
  props = Object.assign({}, props, {
    selectable: true,
    onRowClick: (event, rowNumber) => {
      if (event.target.type !== 'checkbox') {
        event.stopPropagation();
      } else {
        originalOnRowClick(event, rowNumber);
      }
    }
  });
  return <TableRow { ...props } />;
};

SovosSelectableAndClickableTableRow.propTypes = TableRow.propTypes;
export default SovosSelectableAndClickableTableRow;
