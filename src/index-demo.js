import React from 'react';
import { render } from 'react-dom';
import SovosRaisedButtonDemo from './sovos-raised-button/SovosRaisedButton-Example';
import SovosFlatButtonDemo from './sovos-flat-button/SovosFlatButton-Example';
import SovosIconButtonDemo from './sovos-icon-button/SovosIconButton-Example';
import SovosTableCardDemo from './sovos-table-card/SovosTableCard-Example';
import SovosHeroBannerDemo from './sovos-hero-banner/SovosHeroBanner-Example';
import SovosMonthSelectorDemo from './sovos-month-selector/SovosMonthSelector-Example';
import SovosThemeProvider from './sovos-theme-provider/SovosThemeProvider';

const App = () => (
  <SovosThemeProvider>
    <div>
      <SovosMonthSelectorDemo />
      <SovosRaisedButtonDemo />
      <SovosFlatButtonDemo />
      <SovosIconButtonDemo />
      <SovosHeroBannerDemo />
      <SovosTableCardDemo />
    </div>
  </SovosThemeProvider>
);

render(<App />, document.getElementById('root'));
