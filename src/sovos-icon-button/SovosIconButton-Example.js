import React from 'react';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import Home from 'material-ui/svg-icons/action/home';
import Favorite from 'material-ui/svg-icons/action/favorite';
import SovosIconButton from './SovosIconButton';
import { redDark } from '../themes/sovos-colors';

export default () => (
  <Card style={ { textAlign: 'center', margin: '1rem' } }>
    <CardTitle title="Icon Buttons" />
    <CardText>
      <div>
        <SovosIconButton
          size="small"
        >
          <Home />
        </SovosIconButton>

        <SovosIconButton
          size="medium"
        >
          <Home />
        </SovosIconButton>

        <SovosIconButton
          size="large"
        >
          <Home />
        </SovosIconButton>

        <SovosIconButton
          size="large"
          color={ redDark }
        >
          <Favorite />
        </SovosIconButton>
      </div>
      <div>
        <SovosIconButton
          disabled
          size="small"
        >
          <Home />
        </SovosIconButton>

        <SovosIconButton
          disabled
          size="medium"
        >
          <Home />
        </SovosIconButton>

        <SovosIconButton
          disabled
          size="large"
        >
          <Home />
        </SovosIconButton>

        <SovosIconButton
          disabled
          size="large"
          color={ redDark }
        >
          <Favorite />
        </SovosIconButton>
      </div>
    </CardText>
  </Card>
);
