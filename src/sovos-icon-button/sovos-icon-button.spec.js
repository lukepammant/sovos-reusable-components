import React from 'react';
import { mount } from 'enzyme';
import Home from 'material-ui/svg-icons/action/home';
import SovosIconButton from './SovosIconButton';
import SovosThemeProvider from '../sovos-theme-provider/SovosThemeProvider';
import Theme from '../themes/sovos-enterprise';

const renderIconButton = ({ size, color, onClick }) =>
  mount(
    <SovosThemeProvider>
      <SovosIconButton
        size={ size }
        color={ color }
        onClick={ onClick }
      >
        <Home />
      </SovosIconButton>
    </SovosThemeProvider>
  );

describe('SovosIconButton', () => {
  describe('default', () => {
    let wrapper;
    let onClick;

    beforeEach(() => {
      onClick = sinon.spy();
      wrapper = renderIconButton({ onClick });
    });

    it('has "medium" size container', () => {
      const style = wrapper.find('button').prop('style');
      expect(style.width).toEqual(Theme.iconButton.medium.style.width);
    });

    it('has "medium" size icon', () => {
      const style = wrapper.find('svg').prop('style');
      expect(style.width).toEqual(Theme.iconButton.medium.iconStyle.width);
    });

    it('simulates click events', () => {
      wrapper.find('button').simulate('click');
      expect(onClick).toHaveBeenCalled();
    });
  });

  describe('small size', () => {
    let wrapper;

    beforeEach(() => {
      wrapper = renderIconButton({ size: 'small' });
    });

    it('has "small" size container', () => {
      const style = wrapper.find('button').prop('style');
      expect(style.width).toEqual(Theme.iconButton.small.style.width);
    });

    it('has "small" size icon', () => {
      const style = wrapper.find('svg').prop('style');
      expect(style.width).toEqual(Theme.iconButton.small.iconStyle.width);
    });
  });

  describe('medium size', () => {
    let wrapper;

    beforeEach(() => {
      wrapper = renderIconButton({ size: 'medium' });
    });

    it('has "medium" size container', () => {
      const style = wrapper.find('button').prop('style');
      expect(style.width).toEqual(Theme.iconButton.medium.style.width);
    });

    it('has "medium" size icon', () => {
      const style = wrapper.find('svg').prop('style');
      expect(style.width).toEqual(Theme.iconButton.medium.iconStyle.width);
    });
  });

  describe('large size', () => {
    let wrapper;

    beforeEach(() => {
      wrapper = renderIconButton({ size: 'large' });
    });

    it('has "large" size container', () => {
      const style = wrapper.find('button').prop('style');
      expect(style.width).toEqual(Theme.iconButton.large.style.width);
    });

    it('has "large" size icon', () => {
      const style = wrapper.find('svg').prop('style');
      expect(style.width).toEqual(Theme.iconButton.large.iconStyle.width);
    });
  });

  describe('custom color', () => {
    let wrapper;
    const color = '#fefe00';

    beforeEach(() => {
      wrapper = renderIconButton({ color });
    });

    it('has "color" set on icon', () => {
      const style = wrapper.find('svg').prop('style');
      expect(style.color).toEqual(color);
    });
  });
});
