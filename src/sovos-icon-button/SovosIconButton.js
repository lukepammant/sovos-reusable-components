import PropTypes from 'prop-types';
import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import IconButton from 'material-ui/IconButton';
import { fade } from 'material-ui/utils/colorManipulator';

const SovosIconButton = ({ size, color, disabled, style, iconStyle, muiTheme, ...otherProps }) => {
  let styles = {};
  switch (size.toLowerCase()) {
    default:
      console.warn(`Unrecognized size ${size}. Defaulting to 'medium'.`);
      size = 'medium';
    case 'small': case 'medium': case 'large':
      styles = {
        style: {
          ...muiTheme.iconButton.base.style,
          ...muiTheme.iconButton[size].style,
          ...style
        },
        iconStyle: {
          ...muiTheme.iconButton.base.iconStyle,
          ...muiTheme.iconButton[size].iconStyle,
          ...iconStyle
        },
      };
      break;
  }

  if (color) {
    styles.iconStyle.color = color;
  }

  if (disabled) {
    styles.iconStyle.color = fade(styles.iconStyle.color, 0.15);
  }

  return (
    <IconButton
      { ...otherProps }
      { ...styles }
    />
  );
};

SovosIconButton.propTypes = Object.assign({},
  IconButton.propTypes,
  {
    size: PropTypes.oneOf(['small', 'medium', 'large']),
    color: PropTypes.string
  });

SovosIconButton.defaultProps = {
  size: 'medium'
};

export default muiThemeable()(SovosIconButton);
