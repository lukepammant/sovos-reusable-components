export const validateProps = (component, props) => {
  const newProps = { ...props };

  if (typeof (component.propTypes) === 'undefined' ||
    typeof (component.defaultProps) === 'undefined') {
    return newProps;
  }

  for (const prop in newProps) {
    if (component.propTypes[prop] && component.defaultProps[prop]) {
      //TODO: Figure out how to get list of valid types from props
      if (component.propTypes[prop].validValues.indexOf(newProps[prop]) === -1)
        console.warn(`Invalid property value '${prop}'='${newProps[prop]}'. Defaulting to '${component.defaultProps[prop]}'`);
      newProps[prop] = component.defaultProps[prop];
    }
  }

  return newProps;
};