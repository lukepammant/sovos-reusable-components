import React from 'react';
import { mount } from 'enzyme';
import PropTypes from 'prop-types';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import SovosSlidingPanel from './SovosSlidingPanel';
import BlockOverlayLayer from './components/blockOverlayLayer/BlockOverlayLayer';
import ContainerLayer from './components/containerLayer/ContainerLayer';
import CloseDialog from './components/closeDialog/CloseDialog';
import ChildrenMock from './mocks/ChildrenMock';

const renderSovosSlidingPanel = (conf = {}) => {
  const defaultProps = {
    title: 'Some Title',
    propForChildren: 'Some Text for the children',
    open: false,
    onRequestClose: () => {},
    modal: false,
    baseZIndex: 1200,
    overlayTransition: 'fade',
    contentTransition: 'slide',
    showDialogOnClose: false,
    dialogTitle: 'Some Dialog Title',
    dialogText: 'Some Dialog Text',
    showHeaderBar: true,
    contentBackgroundColor: '#333',
    initialLeftPadding: 300,
    additionalButtons: []
  };
  const props = Object.assign({}, defaultProps, conf);
  return mount(
    <SovosSlidingPanel { ...props }>
      <ChildrenMock />
    </SovosSlidingPanel>, {
      context: {
        muiTheme: getMuiTheme()
      },
      childContextTypes: {
        muiTheme: PropTypes.object
      }
    }
  );
};

describe('SovosSlidingPanel', () => {
  describe('General tests', () => {
    it('has a BlockOverlayLayer', () => {
      const wrapper = renderSovosSlidingPanel();
      const container = wrapper.find(BlockOverlayLayer);
      expect(container.exists()).toBe(true);
    });
    it('has a ContainerLayer', () => {
      const wrapper = renderSovosSlidingPanel();
      const container = wrapper.find(ContainerLayer);
      expect(container.exists()).toBe(true);
    });
    it('has a CloseDialog', () => {
      const wrapper = renderSovosSlidingPanel();
      const container = wrapper.find(CloseDialog);
      expect(container.exists()).toBe(true);
    });
  });
  describe('open flag tests', () => {
    it('has a BlockOverlayLayer when open true', () => {
      const wrapper = renderSovosSlidingPanel({ open: true });
      const container = wrapper.find('.sliding-panel__overlay');
      expect(container.exists()).toBe(true);
    });
    it('does not has a BlockOverlayLayer when open false', () => {
      const wrapper = renderSovosSlidingPanel();
      const container = wrapper.find('.sliding-panel__overlay');
      expect(container.exists()).toBe(false);
    });
    it('has a ChildrenMock when open true', () => {
      const wrapper = renderSovosSlidingPanel({ open: true });
      const container = wrapper.find(ChildrenMock);
      expect(container.exists()).toBe(true);
    });
    it('does not has a ChildrenMock when open false', () => {
      const wrapper = renderSovosSlidingPanel();
      const container = wrapper.find(ChildrenMock);
      expect(container.exists()).toBe(false);
    });
    it('has a sliding-panel__child-container when open true', () => {
      const wrapper = renderSovosSlidingPanel({ open: true });
      const container = wrapper.find('.sliding-panel__child-container');
      expect(container.exists()).toBe(true);
    });
    it('does not has a sliding-panel__child-container when open false', () => {
      const wrapper = renderSovosSlidingPanel();
      const container = wrapper.find('.sliding-panel__child-container');
      expect(container.exists()).toBe(false);
    });
    it('hides and shows the children on open prop modification', () => {
      const wrapper = renderSovosSlidingPanel();
      let container = wrapper.find(ChildrenMock);
      expect(container.exists()).toBe(false);
      wrapper.setProps({ open: true });
      container = wrapper.find(ChildrenMock);
      expect(container.exists()).toBe(true);
    });
  });
});
