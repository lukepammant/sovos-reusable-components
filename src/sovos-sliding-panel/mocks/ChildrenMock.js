import React from 'react';
import PropTypes from 'prop-types';
import { RaisedButton } from 'material-ui';

const ChildrenMock = props => (
  <div id="childrenMock">
    <RaisedButton id="closeChildrenMock" label="Close" onTouchTap={ props.onCloseParentSlide } />
    <RaisedButton id="forcedCloseChildrenMock" label="Forced Close" onTouchTap={ props.onForcedCloseParentSlide } />
  </div>
);

ChildrenMock.propTypes = {
  onCloseParentSlide: PropTypes.func,
  onForcedCloseParentSlide: PropTypes.func
};

ChildrenMock.defaultProps = {
  onCloseParentSlide: null,
  onForcedCloseParentSlide: null
};

export default ChildrenMock;
