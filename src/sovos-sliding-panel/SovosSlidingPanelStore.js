const slidingPanelStore = {
  numberOfSlides: 0,
  slides: [],
  isDialogOpen: false,
  slidesWithDialog: [],
  closingFromParent: false
};

export function getDialogOpen() {
  return slidingPanelStore.isDialogOpen;
}

export function getNumberOfSlides() {
  return slidingPanelStore.numberOfSlides;
}

export function resetStore() {
  slidingPanelStore.numberOfSlides = 0;
  slidingPanelStore.slides = [];
  slidingPanelStore.isDialogOpen = false;
  slidingPanelStore.slidesWithDialog = [];
  slidingPanelStore.closingFromParent = false;
}

export function removeSlide(slideNumber, forced) {
  const closeSlide = (index) => {
    slidingPanelStore.numberOfSlides = index;
    if (slidingPanelStore.slides[index].props.showDialogOnClose) slidingPanelStore.slidesWithDialog.pop();
    slidingPanelStore.slides[index].requestClose();
    slidingPanelStore.slides.length = index;
  };

  const hasChildrens = slide => (slidingPanelStore.slides.length > slide + 1);

  const closeSlideChildrens = (slide) => {
    if (hasChildrens(slide)) {
      closeSlide(slide + 1);
    }
  };

  const showSlideDialog = (slide) => {
    slidingPanelStore.isDialogOpen = true;
    slidingPanelStore.slides[slide].showDialog();
  };

  const hideSlideDialog = (slide) => {
    slidingPanelStore.isDialogOpen = false;
    slidingPanelStore.slides[slide].hideDialog();
  };

  const getLastSlideWithDialog = () => (slidingPanelStore.slidesWithDialog[slidingPanelStore.slidesWithDialog.length - 1]);

  const isForcedClose = () => (forced === true);

  const isForcedCloseFromParentSlide = () => (forced === true && slidingPanelStore.closingFromParent !== false);

  const closeLatestDialogSlide = () => {
    const slide = getLastSlideWithDialog();
    closeSlideChildrens(slide);
    showSlideDialog(slide);
  };

  const hasChildrenSlidesWithDialog = slide => (getLastSlideWithDialog() > slide);

  const hasDialog = slide => (slidingPanelStore.slides[slide].props.showDialogOnClose === true);

  if (slidingPanelStore.isDialogOpen) {
    hideSlideDialog(slideNumber);
  }
  if (isForcedCloseFromParentSlide()) {
    forced = false;
    closeSlide(slideNumber);
    slideNumber = slidingPanelStore.closingFromParent;
    slidingPanelStore.closingFromParent = false;
  }
  if (isForcedClose()) {
    closeSlide(slideNumber);
    return;
  }
  if (hasChildrenSlidesWithDialog(slideNumber)) {
    slidingPanelStore.closingFromParent = slideNumber;
    closeLatestDialogSlide(getLastSlideWithDialog());
  } else if (hasDialog(slideNumber)) {
    closeSlideChildrens(slideNumber);
    showSlideDialog(slideNumber);
  } else {
    closeSlide(slideNumber);
  }
}

export function removeSlideFromUnMount(slideNumber) {
  if (slidingPanelStore.numberOfSlides > 0 && slideNumber === 0) removeSlide(slideNumber, true);
}

export function createNewSlide(slide) {
  if (slide.props.showDialogOnClose) slidingPanelStore.slidesWithDialog.push(slidingPanelStore.numberOfSlides);
  slidingPanelStore.numberOfSlides += 1;
  slidingPanelStore.slides.push(slide);
}

export function resetDialogOpen() {
  slidingPanelStore.closingFromParent = false;
  slidingPanelStore.isDialogOpen = false;
}
