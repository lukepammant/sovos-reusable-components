import React from 'react';
import PropTypes from 'prop-types';
import SvgIcon from 'material-ui/SvgIcon';
import { RaisedButton } from 'material-ui';

const CloseIcon = props => (
  <SvgIcon { ...props } >
    <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z" />
  </SvgIcon>
);

class SlidingPanelHeader extends React.Component {
  componentWillMount() {
    this.hoverColor = '#404040';
    const additionalButtonsProps = {
      hoverColor: this.hoverColor
    };
    this.additionalButtons = this.props.additionalButtons.map((button, index) => (
      React.cloneElement(button, Object.assign({ key: index }, additionalButtonsProps)))
    );
  }

  handleCloseClick = () => {
    this.props.onCloseClick();
  };

  headerContainerStyle = {
    zIndex: this.props.zIndex,
    borderBottom: this.props.showHeaderBorderBottom ? '1px solid #ebeef0' : 'none',
    height: (this.props.actionButtons.length === 0) ? 50 : 100
  };

  render() {
    return (
      <div className="sliding-panel__header" style={ this.headerContainerStyle }>
        <div className="sliding-panel__header-row1">
          <div className="sliding-panel__header-title">{ this.props.title }</div>
          <div className="sliding-panel__header-icon-container">
            { this.additionalButtons }
            <CloseIcon className="sliding-panel__header-icon" onTouchTap={ this.handleCloseClick } hoverColor={ this.hoverColor } />
          </div>
        </div>
        <div className="sliding-panel__header-row2">
          <div className="sliding-panel__stepper-container" />
          <div className="sliding-panel__action-container">
            {
              this.props.actionButtons.map((buttonConf, index) => (
                <RaisedButton secondary { ...buttonConf } key={ index } />
              ))
            }
          </div>
        </div>
      </div>
    );
  }
}

SlidingPanelHeader.propTypes = {
  title: PropTypes.string,
  onCloseClick: PropTypes.func.isRequired,
  zIndex: PropTypes.number.isRequired,
  additionalButtons: PropTypes.array,
  showHeaderBorderBottom: PropTypes.bool.isRequired,
  actionButtons: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    callback: PropTypes.func,
    style: PropTypes.object
  })).isRequired
};

SlidingPanelHeader.defaultProps = {
  title: '',
  additionalButtons: []
};

export default SlidingPanelHeader;
