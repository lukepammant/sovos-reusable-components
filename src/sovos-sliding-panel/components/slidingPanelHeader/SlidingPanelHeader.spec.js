import React from 'react';
import { shallow } from 'enzyme';
import SvgIcon from 'material-ui/SvgIcon';
import SlidingPanelHeader from './SlidingPanelHeader';

const ExportIcon = props => (
  <SvgIcon { ...props } >
    <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z" />
  </SvgIcon>
);

const RefreshIcon = props => (
  <SvgIcon { ...props } >
    <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z" />
  </SvgIcon>
);

const renderSlidingPanelHeader = (conf = {}) => {
  const defaultProps = {
    zIndex: 1200,
    onCloseClick: () => null,
    actionButtons: [],
    showHeaderBorderBottom: true
  };
  const props = Object.assign({}, defaultProps, conf);
  return shallow(<SlidingPanelHeader { ...props } />);
};

describe('SlidingPanelHeader', () => {
  describe('General Tests', () => {
    it('has no title', () => {
      const wrapper = renderSlidingPanelHeader();
      const container = wrapper.find('.sliding-panel__header-title');
      expect(container.text()).toEqual('');
    });
    it('has title of the props', () => {
      const wrapper = renderSlidingPanelHeader({ title: 'Test Title' });
      const container = wrapper.find('.sliding-panel__header-title');
      expect(container.text()).toEqual('Test Title');
    });
    it('has zIndex of the props', () => {
      const wrapper = renderSlidingPanelHeader({ title: 'Test Title' });
      const container = wrapper.find('.sliding-panel__header');
      expect(container.get(0).props.style.zIndex).toEqual(1200);
    });
    it('has borderBottom when showHeaderBorderBottom to true', () => {
      const wrapper = renderSlidingPanelHeader({ title: 'Test Title' });
      const container = wrapper.find('.sliding-panel__header');
      expect(container.get(0).props.style.borderBottom).toEqual('1px solid #ebeef0');
    });
    it('doesnt has borderBottom when showHeaderBorderBottom to false', () => {
      const wrapper = renderSlidingPanelHeader({ title: 'Test Title', showHeaderBorderBottom: false });
      const container = wrapper.find('.sliding-panel__header');
      expect(container.get(0).props.style.borderBottom).toEqual('none');
    });
    it('calls the callback onCloseClick', () => {
      const closeClick = sinon.spy();
      const wrapper = renderSlidingPanelHeader({ onCloseClick: closeClick });
      const container = wrapper.find('CloseIcon.sliding-panel__header-icon');
      container.simulate('touchTap');
      expect(closeClick.callCount).toEqual(1);
    });
  });
  describe('additionalButtons', () => {
    it('has additional Buttons', () => {
      const additionalButtons = [
        <RefreshIcon className="sliding-panel__header-icon" onTouchTap={ () => {} } key="refresh" />,
        <ExportIcon className="sliding-panel__header-icon" onTouchTap={ () => {} } key="export" />
      ];
      const wrapper = renderSlidingPanelHeader({ additionalButtons });
      const container = wrapper.find('.sliding-panel__header-icon');
      expect(container.length).toEqual(3);
    });
    it('has additional Buttons with callbacks', () => {
      const refreshClick = sinon.spy();
      const additionalButtons = [
        <RefreshIcon className="sliding-panel__header-icon" onTouchTap={ refreshClick } key="refresh" />,
      ];
      const wrapper = renderSlidingPanelHeader({ additionalButtons });
      const container = wrapper.find('RefreshIcon.sliding-panel__header-icon');
      container.simulate('touchTap');
      expect(refreshClick.callCount).toEqual(1);
    });
  });
  describe('actionButtons', () => {
    it('has additional Buttons with custom label', () => {
      const actionButtons = [
        { label: 'Action 1', onTouchTap: () => null },
        { label: 'Action 2', onTouchTap: () => null }
      ];
      const wrapper = renderSlidingPanelHeader({ actionButtons });
      const container = wrapper.find('RaisedButton');
      expect(container.length).toEqual(2);
      expect(container.at(0).prop('label')).toEqual('Action 1');
      expect(container.at(1).prop('label')).toEqual('Action 2');
    });
    it('has additional Buttons with onTouchTaps', () => {
      const actionClick = sinon.spy();
      const actionButtons = [
        { label: 'Action 1', onTouchTap: actionClick }
      ];
      const wrapper = renderSlidingPanelHeader({ actionButtons });
      const container = wrapper.find('RaisedButton');
      container.at(0).simulate('touchTap');
      expect(actionClick.callCount).toEqual(1);
    });
  });
});
