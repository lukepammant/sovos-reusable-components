import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  FlatButton
} from 'material-ui';

const CloseDialog = ({ open, onOKClick, onCancelClick, dialogTitle, dialogText }) => {
  const actions = [
    <FlatButton label="Cancel" onTouchTap={ onCancelClick } />,
    <FlatButton label="Ok" onTouchTap={ onOKClick } />
  ];

  return (
    <div>
      <Dialog
        title={ dialogTitle }
        actions={ actions }
        open={ open }
        modal
      >
        { dialogText }
      </Dialog>
    </div>
  );
};

CloseDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onOKClick: PropTypes.func.isRequired,
  onCancelClick: PropTypes.func.isRequired,
  dialogTitle: PropTypes.string.isRequired,
  dialogText: PropTypes.string.isRequired
};

export default CloseDialog;
