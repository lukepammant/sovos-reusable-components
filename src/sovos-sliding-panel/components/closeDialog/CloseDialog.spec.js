import React from 'react';
import { shallow } from 'enzyme';
import CloseDialog from './CloseDialog';

const getCloseDialog = (conf = {}) => {
  const defaultProps = {
    open: false,
    onOKClick: () => {},
    onCancelClick: () => {},
    dialogTitle: 'Some title',
    dialogText: 'Some Text'
  };
  const props = Object.assign({}, defaultProps, conf);
  return shallow(
    <CloseDialog { ...props } />
  );
};

describe('CloseDialog', () => {
  describe('General tests', () => {
    it('has open flag in false when passed', () => {
      const wrapper = getCloseDialog();
      const container = wrapper.find('Dialog');
      expect(container.get(0).props.open).toEqual(false);
    });
    it('has open flag true when passed', () => {
      const wrapper = getCloseDialog({ open: true });
      const container = wrapper.find('Dialog');
      expect(container.get(0).props.open).toEqual(true);
    });
    it('has title when passed dialogTitle', () => {
      const wrapper = getCloseDialog();
      const container = wrapper.find('Dialog');
      expect(container.get(0).props.title).toEqual('Some title');
    });
    it('has dialog text when passed dialogText', () => {
      const wrapper = getCloseDialog();
      const container = wrapper.find('Dialog');
      expect(container.get(0).props.children).toEqual('Some Text');
    });
  });
});
