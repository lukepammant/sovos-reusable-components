import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransitionGroup } from 'react-transition-group';
import SlidingPanelHeader from '../slidingPanelHeader/SlidingPanelHeader';

const ContainerLayer = ({ order, contentBackgroundColor, zIndex, contentTransition, children, open, showHeaderBar, title, onRequestClose, initialLeftPadding, additionalButtons, actionButtons, showHeaderBorderBottom }) => {
  const contentEffectTime = 400;

  const handleContainerClick = (event) => {
    event.stopPropagation();
  };

  const childContainerStyle = {
    zIndex,
    backgroundColor: contentBackgroundColor,
    left: (order === 0) ? initialLeftPadding : initialLeftPadding + (50 * order)
  };

  const scrollingLayerStyle = {
    top: (actionButtons.length === 0) ? 50 : 100
  };

  return (
    <CSSTransitionGroup transitionName={ contentTransition } transitionEnterTimeout={ contentEffectTime } transitionLeaveTimeout={ contentEffectTime }>
      {open &&
      <div className="sliding-panel__child-container" style={ childContainerStyle } onTouchTap={ handleContainerClick }>
        {
          showHeaderBar &&
          <SlidingPanelHeader
            title={ title }
            onCloseClick={ onRequestClose }
            zIndex={ zIndex }
            additionalButtons={ additionalButtons }
            actionButtons={ actionButtons }
            showHeaderBorderBottom={ showHeaderBorderBottom }
          />
        }
        <div className="sliding-panel__scrolling-layer" style={ scrollingLayerStyle }>
          {children}
        </div>
      </div>
      }
    </CSSTransitionGroup>
  );
};

ContainerLayer.propTypes = {
  zIndex: PropTypes.number.isRequired,
  children: PropTypes.element.isRequired,
  contentTransition: PropTypes.string.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  order: PropTypes.number.isRequired,
  showHeaderBar: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  contentBackgroundColor: PropTypes.string.isRequired,
  initialLeftPadding: PropTypes.number.isRequired,
  additionalButtons: PropTypes.array,
  actionButtons: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    callback: PropTypes.func,
    style: PropTypes.object
  })).isRequired,
  showHeaderBorderBottom: PropTypes.bool.isRequired
};

ContainerLayer.defaultProps = {
  additionalButtons: []
};

export default ContainerLayer;
