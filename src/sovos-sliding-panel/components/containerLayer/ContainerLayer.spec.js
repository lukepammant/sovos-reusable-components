import React from 'react';
import { shallow } from 'enzyme';
import ContainerLayer from './ContainerLayer';

const getContainerLayer = (conf = {}) => {
  const defaultProps = {
    order: 0,
    contentBackgroundColor: '#222',
    contentTransition: 'slide',
    open: true,
    showHeaderBar: true,
    title: 'Some Title',
    zIndex: 1200,
    onRequestClose: () => {},
    onRequestForcedClose: () => {},
    initialLeftPadding: 300,
    additionalButtons: [],
    actionButtons: [],
    showHeaderBorderBottom: true
  };
  const props = Object.assign({}, defaultProps, conf);
  return shallow(
    <ContainerLayer { ...props } >
      <div id="childrenID" />
    </ContainerLayer>
  );
};

describe('ContainerLayer', () => {
  describe('style tests', () => {
    it('has left = initialSpacing when order is 0', () => {
      const wrapper = getContainerLayer();
      const container = wrapper.find('.sliding-panel__child-container');
      expect(container.get(0).props.style.left).toEqual(300);
    });
    it('has left = initialSpacing + order when order is > 0', () => {
      const wrapper = getContainerLayer({ order: 2 });
      const container = wrapper.find('.sliding-panel__child-container');
      expect(container.get(0).props.style.left).toEqual(400);
    });
    it('has background color from props', () => {
      const wrapper = getContainerLayer();
      const container = wrapper.find('.sliding-panel__child-container');
      expect(container.get(0).props.style.backgroundColor).toEqual('#222');
    });
    it('has zIndex from props', () => {
      const wrapper = getContainerLayer();
      const container = wrapper.find('.sliding-panel__child-container');
      expect(container.get(0).props.style.zIndex).toEqual(1200);
    });
  });
  describe('open tests', () => {
    it('renders the component on open', () => {
      const wrapper = getContainerLayer();
      const container = wrapper.find('.sliding-panel__child-container');
      expect(container.length).toEqual(1);
    });
    it('does not render the component on open', () => {
      const wrapper = getContainerLayer({ open: false });
      const container = wrapper.find('.sliding-panel__child-container');
      expect(container.length).toEqual(0);
    });
  });
  describe('Transition tests', () => {
    it('has the transition passed as props', () => {
      const wrapper = getContainerLayer();
      const container = wrapper.find('CSSTransitionGroup');
      expect(container.get(0).props.transitionName).toEqual('slide');
    });
  });
  describe('Children tests', () => {
    it('has children as part of the component', () => {
      const wrapper = getContainerLayer();
      const container = wrapper.find('#childrenID');
      expect(container.length).toEqual(1);
    });
  });
  describe('onRequestClose tests', () => {
    let closeClick;
    beforeEach(() => {
      closeClick = sinon.spy();
    });
    it('Is calling to the click by default', () => {
      const wrapper = getContainerLayer({ onRequestClose: closeClick });
      const container = wrapper.find('.sliding-panel__child-container');
      container.get(0).props.children[0].props.onCloseClick();
      expect(closeClick.callCount).toEqual(1);
    });
  });
  describe('SlidingHeader tests', () => {
    it('Exists on showHeaderBar', () => {
      const wrapper = getContainerLayer();
      const container = wrapper.find('.sliding-panel__child-container').get(0).props.children[0];
      expect(typeof container).toEqual('object');
    });
    it('Does not exists on showHeaderBar false', () => {
      const wrapper = getContainerLayer({ showHeaderBar: false });
      const container = wrapper.find('.sliding-panel__child-container').get(0).props.children[0];
      expect(container).toEqual(false);
    });
    it('Has title passed as parameter', () => {
      const wrapper = getContainerLayer();
      const container = wrapper.find('.sliding-panel__child-container').get(0).props.children[0];
      expect(container.props.title).toEqual('Some Title');
    });
    it('Has zIndex passed as parameter', () => {
      const wrapper = getContainerLayer();
      const container = wrapper.find('.sliding-panel__child-container').get(0).props.children[0];
      expect(container.props.zIndex).toEqual(1200);
    });
    it('Has additionalButtons passed as parameter', () => {
      const wrapper = getContainerLayer({ additionalButtons: [{}, {}, {}] });
      const container = wrapper.find('.sliding-panel__child-container').get(0).props.children[0];
      expect(container.props.additionalButtons.length).toEqual(3);
    });
  });
});
