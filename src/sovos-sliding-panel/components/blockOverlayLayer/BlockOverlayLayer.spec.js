import React from 'react';
import { shallow } from 'enzyme';
import BlockOverlayLayer from './BlockOverlayLayer';

const getBlockOverlayLayer = (conf = {}) => {
  const defaultProps = {
    overlayTransition: 'fade',
    modal: false,
    order: 0,
    open: true,
    onRequestClose: () => {},
    zIndex: 1200,
    initialLeftPadding: 300
  };

  const props = Object.assign({}, defaultProps, conf);
  return shallow(
    <BlockOverlayLayer { ...props } />
  );
};

describe('BlockOverlayLayer', () => {
  describe('Open tests', () => {
    it('Draws blocking layer on open', () => {
      const wrapper = getBlockOverlayLayer();
      const container = wrapper.find('.sliding-panel__blocking-layer_all-screen');
      expect(container).toBePresent();
    });
    it('Doesnt draws blocking layer on open = false', () => {
      const wrapper = getBlockOverlayLayer({ open: false });
      const container = wrapper.find('.sliding-panel__blocking-layer_all-screen');
      expect(container).not.toBePresent();
    });
  });
  describe('left spacing on blocking div tests', () => {
    it('has left 0 on order 0', () => {
      const wrapper = getBlockOverlayLayer();
      const container = wrapper.find('.sliding-panel__blocking-layer_all-screen');
      expect(container.get(0).props.style.left).toEqual(0);
    });
    it('has left 0 on modal', () => {
      const wrapper = getBlockOverlayLayer({ order: 1, modal: true });
      const container = wrapper.find('.sliding-panel__blocking-layer_all-screen');
      expect(container.get(0).props.style.left).toEqual(0);
    });
    it('has left equals to left padding * order + 50 on order = 1', () => {
      const wrapper = getBlockOverlayLayer({ order: 1 });
      const container = wrapper.find('.sliding-panel__blocking-layer_current-slide');
      expect(container.get(0).props.style.left).toEqual(350);
    });
    it('has left equals to left padding * order + 50 on order > 1', () => {
      const wrapper = getBlockOverlayLayer({ order: 3 });
      const container = wrapper.find('.sliding-panel__blocking-layer_current-slide');
      expect(container.get(0).props.style.left).toEqual(450);
    });
  });
  describe('blocking layer clases', () => {
    it('has sliding-panel__blocking-layer_all-screen order 0', () => {
      const wrapper = getBlockOverlayLayer();
      const container = wrapper.find('.sliding-panel__blocking-layer_all-screen');
      expect(container.length).toEqual(1);
    });
    it('has sliding-panel__blocking-layer_all-screen on modal', () => {
      const wrapper = getBlockOverlayLayer({ order: 1, modal: true });
      const container = wrapper.find('.sliding-panel__blocking-layer_all-screen');
      expect(container.length).toEqual(1);
    });
    it('has sliding-panel__blocking-layer_current-slide on order > 1', () => {
      const wrapper = getBlockOverlayLayer({ order: 3 });
      const container = wrapper.find('.sliding-panel__blocking-layer_current-slide');
      expect(container.length).toEqual(1);
    });
  });
  describe('left spacing on overlay div tests', () => {
    it('has left 0 on order 0', () => {
      const wrapper = getBlockOverlayLayer();
      const container = wrapper.find('.sliding-panel__overlay');
      expect(container.get(0).props.style.left).toEqual(0);
    });
    it('has left equal to leftSpacing on order 1', () => {
      const wrapper = getBlockOverlayLayer({ order: 1 });
      const container = wrapper.find('.sliding-panel__overlay');
      expect(container.get(0).props.style.left).toEqual(300);
    });
    it('has left equals to leftSpacing + 50 on order > 1', () => {
      const wrapper = getBlockOverlayLayer({ order: 2 });
      const container = wrapper.find('.sliding-panel__overlay');
      expect(container.get(0).props.style.left).toEqual(350);
    });
  });
  describe('zIndex tests', () => {
    it('Blocking layer has zIndex from props', () => {
      const wrapper = getBlockOverlayLayer();
      const container = wrapper.find('.sliding-panel__blocking-layer_all-screen');
      expect(container.get(0).props.style.zIndex).toEqual(1200);
    });
    it('Overlay layer has zIndex from props', () => {
      const wrapper = getBlockOverlayLayer();
      const container = wrapper.find('.sliding-panel__overlay');
      expect(container.get(0).props.style.zIndex).toEqual(1200);
    });
  });
  describe('onRequestClose tests', () => {
    let closeClick;
    beforeEach(() => {
      closeClick = sinon.spy();
    });
    it('Is calling to the click by default', () => {
      const wrapper = getBlockOverlayLayer({ onRequestClose: closeClick });
      const container = wrapper.find('.sliding-panel__blocking-layer_all-screen');
      container.simulate('touchTap');
      expect(closeClick).toHaveBeenCalled();
    });
    it('Does not call to the click by default', () => {
      const wrapper = getBlockOverlayLayer({ onRequestClose: closeClick, modal: true });
      const container = wrapper.find('.sliding-panel__blocking-layer_all-screen');
      container.simulate('touchTap');
      expect(closeClick).not.toHaveBeenCalled();
    });
  });
  describe('overlayTransition tests', () => {
    it('Is using the transition from the props', () => {
      const wrapper = getBlockOverlayLayer({ overlayTransition: 'slide' });
      const container = wrapper.find('CSSTransitionGroup');
      expect(container.get(0).props.transitionName).toEqual('slide');
    });
  });
});
