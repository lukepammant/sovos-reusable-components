import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransitionGroup } from 'react-transition-group';

const BlockOverlay = ({ overlayTransition, modal, onRequestClose, zIndex, open, order, initialLeftPadding }) => {
  const overlayEffectTime = 300;

  const overlayStyle = {
    zIndex,
    left: (order === 0) ? 0 : initialLeftPadding + (50 * (order - 1))
  };

  const blockingStyle = {
    zIndex,
    left: (order === 0 || modal) ? 0 : initialLeftPadding + (order * 50)
  };

  const blockingClass = `sliding-panel__blocking-layer_${(order === 0 || modal) ? 'all-screen' : 'current-slide'}`;

  return (
    <CSSTransitionGroup transitionName={ overlayTransition } transitionEnterTimeout={ overlayEffectTime } transitionLeaveTimeout={ overlayEffectTime }>
      {open &&
        <div className={ blockingClass } style={ blockingStyle } onTouchTap={ !modal && onRequestClose }>
          <div className="sliding-panel__overlay" style={ overlayStyle } />
        </div>
      }
    </CSSTransitionGroup>
  );
};

BlockOverlay.propTypes = {
  open: PropTypes.bool.isRequired,
  order: PropTypes.number.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  modal: PropTypes.bool.isRequired,
  overlayTransition: PropTypes.string.isRequired,
  zIndex: PropTypes.number.isRequired,
  initialLeftPadding: PropTypes.number.isRequired
};

export default BlockOverlay;
