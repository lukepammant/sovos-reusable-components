import React, { Component } from 'react';
import PropTypes from 'prop-types';
import BlockOverlayLayer from './components/blockOverlayLayer/BlockOverlayLayer';
import ContainerLayer from './components/containerLayer/ContainerLayer';
import CloseDialog from './components/closeDialog/CloseDialog';
import './sovosSlidingPanel.css';
import * as SlidingPanelStore from './SovosSlidingPanelStore';

/**
 * *Sliding Panel* component is a container used in UIs the needs implement different detail levels.
 */
class SovosSlidingPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openDialog: false
    };
    this.order = SlidingPanelStore.getNumberOfSlides();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.open !== nextProps.open) {
      if (nextProps.open === true) {
        SlidingPanelStore.createNewSlide(Object.assign({}, {
          requestClose: this.requestClose,
          showDialog: this.showDialog,
          hideDialog: this.hideDialog,
          props: this.props
        }));
      }
    }
  }

  componentWillUnmount() {
    SlidingPanelStore.removeSlideFromUnMount(this.order, true);
  }

  getZIndex = () => (this.props.open ? this.props.baseZIndex + (this.order * 2) : 0);

  handleCancelDialog = () => {
    SlidingPanelStore.resetDialogOpen();
    this.hideDialog();
  };

  hideDialog = () => {
    this.setState({ openDialog: false });
  };

  showDialog = () => {
    this.setState({ openDialog: true });
  };

  handleCloseDialog = () => {
    SlidingPanelStore.removeSlide(this.order, true);
  };

  requestClose = () => {
    if (this.props.onRequestClose) {
      this.props.onRequestClose();
    }
  };

  callCloseReducer = () => {
    SlidingPanelStore.removeSlide(this.order, false);
  };

  callForcedCloseReducer = () => {
    SlidingPanelStore.removeSlide(this.order, true);
  };

  render() {
    const { children, dialogTitle, dialogText, contentBackgroundColor, contentTransition, open, showHeaderBar, title, overlayTransition, modal, initialLeftPadding } = this.props;
    const childrenWithProps = React.cloneElement(children, {
      onCloseParentSlide: this.callCloseReducer,
      onForcedCloseParentSlide: this.callForcedCloseReducer
    });
    const zIndex = this.getZIndex();

    return (
      <span>
        <BlockOverlayLayer
          overlayTransition={ overlayTransition }
          modal={ modal }
          order={ this.order }
          open={ open }
          onRequestClose={ this.callCloseReducer }
          zIndex={ zIndex }
          initialLeftPadding={ initialLeftPadding }
        />
        <ContainerLayer
          order={ this.order }
          contentBackgroundColor={ contentBackgroundColor }
          contentTransition={ contentTransition }
          open={ open }
          showHeaderBar={ showHeaderBar }
          title={ title }
          zIndex={ zIndex }
          onRequestClose={ this.callCloseReducer }
          onRequestForcedClose={ this.callForcedCloseReducer }
          initialLeftPadding={ initialLeftPadding }
          additionalButtons={ this.props.additionalButtons }
          actionButtons={ this.props.actionButtons }
          showHeaderBorderBottom={ this.props.showHeaderBorderBottom }
        >
          { childrenWithProps }
        </ContainerLayer>
        <CloseDialog open={ this.state.openDialog } dialogTitle={ dialogTitle } onOKClick={ this.handleCloseDialog } onCancelClick={ this.handleCancelDialog } dialogText={ dialogText } />
      </span>
    );
  }
}

SovosSlidingPanel.propTypes = {
  /**
   * Children react prop is marked as required because this component is a container.
   *
   * @ignore
   */
  children: PropTypes.element.isRequired,
  /** If open is true Sliding Panel will be displayed. */
  open: PropTypes.bool.isRequired,
  /** Callback called when this panel is being closed. */
  onRequestClose: PropTypes.func,
  /** Indicate if user can close this panel doing click outside it. */
  modal: PropTypes.bool,
  /** Title of the panel. */
  title: PropTypes.string,
  /** Used if the your app works with high levels of z-index. */
  baseZIndex: PropTypes.number,
  /** Name of the effect that the overlay layer uses when appear. Two posible values:  slide or fade */
  overlayTransition: PropTypes.string,
  /** Name of the effect that the container uses when appear. Two posible values:  slide or fade */
  contentTransition: PropTypes.string,
  /** Shows a confirmation dialog when sliding panel is closing. */
  showDialogOnClose: PropTypes.bool, // eslint-disable-line react/no-unused-prop-types
  /** Title for dialog showed if showDialogOnClose is true */
  dialogTitle: PropTypes.string,
  /** Message of dialog showed if showDialogOnClose is true */
  dialogText: PropTypes.string,
  /** Shows a header in the container. */
  showHeaderBar: PropTypes.bool,
  /** Background color of the sliding panel. */
  contentBackgroundColor: PropTypes.string,
  /** Distance in pixels from left where first sliding panel will appear. */
  initialLeftPadding: PropTypes.number,
  /** Array of buttons that will appear in header bar. */
  additionalButtons: PropTypes.array,
  /** Array of Action buttons that will be placed on the header, should pass { label: 'button label, callback: functionCallback, style: overwrite style for the button root element } */
  actionButtons: PropTypes.arrayOf(PropTypes.object),
  /** Controls if it will be draw a border that will separate the Sliding panel header from the content. */
  showHeaderBorderBottom: PropTypes.bool
};

SovosSlidingPanel.defaultProps = {
  onRequestClose: null,
  modal: false,
  title: '',
  baseZIndex: 1310,
  overlayTransition: 'fade',
  contentTransition: 'slide',
  showDialogOnClose: false,
  dialogTitle: 'Information not saved',
  dialogText: 'Are you sure you want to close this slide, without saving the information?.',
  showHeaderBar: true,
  contentBackgroundColor: '#fff',
  initialLeftPadding: 280,
  additionalButtons: [],
  actionButtons: [],
  showHeaderBorderBottom: true
};

export default SovosSlidingPanel;
