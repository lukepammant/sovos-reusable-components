import * as SlidingPanelModule from './SovosSlidingPanelStore';

const generateSlide = (conf = {}) => {
  const defaultConf = {
    props: {
      showDialogOnClose: false
    },
    requestClose: () => null,
    showDialog: () => null,
    hideDialog: () => null
  };
  const slide = Object.assign({}, defaultConf, conf);
  return slide;
};

describe('SlidingPanelModule > ', () => {
  beforeEach(() => {
    SlidingPanelModule.resetStore();
  });
  describe('Adding slides > ', () => {
    it('Has one slide', () => {
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(0);
      SlidingPanelModule.createNewSlide(generateSlide());
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(1);
    });
    it('Has 5 slides', () => {
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(0);
      SlidingPanelModule.createNewSlide(generateSlide());
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(1);
      SlidingPanelModule.createNewSlide(generateSlide());
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(2);
      SlidingPanelModule.createNewSlide(generateSlide());
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(3);
      SlidingPanelModule.createNewSlide(generateSlide());
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(4);
      SlidingPanelModule.createNewSlide(generateSlide());
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(5);
    });
  });
  describe('removing slides with no dialogs > ', () => {
    beforeEach(() => {
      SlidingPanelModule.createNewSlide(generateSlide());
      SlidingPanelModule.createNewSlide(generateSlide());
      SlidingPanelModule.createNewSlide(generateSlide());
      SlidingPanelModule.createNewSlide(generateSlide());
      SlidingPanelModule.createNewSlide(generateSlide());
    });
    it('Removes all the slides when closing the first one', () => {
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(5);
      SlidingPanelModule.removeSlide(0, false);
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(0);
    });
    it('Removes all the slides but the first and second when closing the third one', () => {
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(5);
      SlidingPanelModule.removeSlide(2, false);
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(2);
    });
  });
  describe('removing slides with dialogs > ', () => {
    beforeEach(() => {
      SlidingPanelModule.createNewSlide(generateSlide());
      SlidingPanelModule.createNewSlide(generateSlide());
      SlidingPanelModule.createNewSlide(generateSlide({ props: { showDialogOnClose: true } }));
      SlidingPanelModule.createNewSlide(generateSlide());
      SlidingPanelModule.createNewSlide(generateSlide({ props: { showDialogOnClose: true } }));
      SlidingPanelModule.createNewSlide(generateSlide());
    });
    it('Stops the remove of the slides on the first one with dialog', () => {
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(6);
      expect(SlidingPanelModule.getDialogOpen()).toBe(false);
      SlidingPanelModule.removeSlide(0, false);
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(5);
      expect(SlidingPanelModule.getDialogOpen()).toBe(true);
      SlidingPanelModule.removeSlide(0, false);
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(5);
    });
    it('Stops the remove and resumes the removal of the slides on the forced closing', () => {
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(6);
      SlidingPanelModule.removeSlide(0, false);
      expect(SlidingPanelModule.getDialogOpen()).toBe(true);
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(5);
      SlidingPanelModule.removeSlide(4, true);
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(3);
      expect(SlidingPanelModule.getDialogOpen()).toBe(true);
      SlidingPanelModule.removeSlide(2, true);
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(0);
    });
    it('Stops the remove and resumes the removal of the slides on the forced closing for the first one', () => {
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(6);
      SlidingPanelModule.removeSlide(1, false);
      expect(SlidingPanelModule.getDialogOpen()).toBe(true);
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(5);
      SlidingPanelModule.removeSlide(4, true);
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(3);
      expect(SlidingPanelModule.getDialogOpen()).toBe(true);
      SlidingPanelModule.removeSlide(2, true);
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(1);
    });
  });
  describe('removing slides with dialog on the last one > ', () => {
    beforeEach(() => {
      SlidingPanelModule.createNewSlide(generateSlide());
      SlidingPanelModule.createNewSlide(generateSlide());
      SlidingPanelModule.createNewSlide(generateSlide({ props: { showDialogOnClose: true } }));
    });
    it('Stops the remove of the slides on the last slide that has dialog', () => {
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(3);
      expect(SlidingPanelModule.getDialogOpen()).toBe(false);
      SlidingPanelModule.removeSlide(0, false);
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(3);
      expect(SlidingPanelModule.getDialogOpen()).toBe(true);
      SlidingPanelModule.removeSlide(0, false);
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(3);
      expect(SlidingPanelModule.getDialogOpen()).toBe(true);
    });
    it('Stops and resumes on the removal of the slides after forced of the last with dialog', () => {
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(3);
      expect(SlidingPanelModule.getDialogOpen()).toBe(false);
      SlidingPanelModule.removeSlide(0, false);
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(3);
      expect(SlidingPanelModule.getDialogOpen()).toBe(true);
      SlidingPanelModule.removeSlide(2, true);
      expect(SlidingPanelModule.getNumberOfSlides()).toEqual(0);
      expect(SlidingPanelModule.getDialogOpen()).toBe(false);
    });
  });
});
