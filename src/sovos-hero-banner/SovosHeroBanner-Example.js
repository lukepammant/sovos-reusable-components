import React from 'react';
import { Card } from 'material-ui/Card';
import SovosHeroBanner from './SovosHeroBanner';
import { redLight } from '../themes/sovos-colors';

export default () => (
  <Card style={ { margin: '1rem', padding: '10px' } }>
    <SovosHeroBanner
      parentPadding={ { top: 10, right: 10, left: 10 } }
      title="Massachusetts Sales and Use Tax"
      subtitle="Acme Corp"
      subtitle2="$2,941.32 Due"
    />
    <p style={ { margin: '15px' } }>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi aliquam possimus harum similique enim? Impedit voluptas dignissimos ut aut magnam illo omnis quisquam quaerat placeat, sapiente vero assumenda! Ratione, at.
    </p>
    <SovosHeroBanner
      parentPadding={ { right: 10, left: 10 } }
      title="Sans subtitles"
      backgroundImage="url('http://www.templera.com/wp-content/uploads/edd/2015/04/Banner-Background-006.jpg')"
    />
    <p style={ { margin: '15px' } }>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi aliquam possimus harum similique enim? Impedit voluptas dignissimos ut aut magnam illo omnis quisquam quaerat placeat, sapiente vero assumenda! Ratione, at.
    </p>
    <SovosHeroBanner
      parentPadding={ { bottom: 10, right: 10, left: 10 } }
      textColor={ redLight }
      title="Bottom hero"
      subtitle="You just never know..."
    />
  </Card>
);
