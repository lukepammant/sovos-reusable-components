import React from 'react';
import { mount } from 'enzyme';
import SovosHeroBanner from './SovosHeroBanner';
import SovosThemeProvider from '../sovos-theme-provider/SovosThemeProvider';
import Theme from '../themes/sovos-enterprise';

const renderHeroBanner = ({ subtitle, subtitle2, containerStyle, textStyle, parentPadding }) =>
  mount(
    <SovosThemeProvider>
      <SovosHeroBanner
        title="Testing"
        subtitle={ subtitle }
        subtitle2={ subtitle2 }
        containerStyle={ containerStyle }
        textStyle={ textStyle }
        parentPadding={ parentPadding }
      />
    </SovosThemeProvider>
  );

describe('SovosHeroBanner', () => {
  describe('default', () => {
    let wrapper;

    beforeEach(() => {
      wrapper = renderHeroBanner({ subtitle: 'testing', subtitle2: 'testing' });
    });

    it('has BackgroundImage set', () => {
      const style = wrapper.find('div').prop('style');
      expect(style.backgroundImage).toBeDefined();
    });

    describe('title', () => {
      it('has color set according to theme', () => {
        const style = wrapper.find('span').at(0).prop('style');
        expect(style.color).toEqual(Theme.heroBanner.title.color);
      });

      it('has font-size set according to theme', () => {
        const style = wrapper.find('span').at(0).prop('style');
        expect(style.fontSize).toEqual(Theme.heroBanner.title.fontSize);
      });
    });

    describe('subtitle', () => {
      it('has color set according to theme', () => {
        const style = wrapper.find('span').at(1).prop('style');
        expect(style.color).toEqual(Theme.heroBanner.subtitles.color);
      });

      it('has text font-size set according to theme', () => {
        const style = wrapper.find('span').at(1).prop('style');
        expect(style.fontSize).toEqual(Theme.heroBanner.subtitles.fontSize);
      });
    });

    describe('subtitle2', () => {
      it('has color set according to theme', () => {
        const style = wrapper.find('span').at(2).prop('style');
        expect(style.color).toEqual(Theme.heroBanner.subtitles.color);
      });

      it('has text font-size set according to theme', () => {
        const style = wrapper.find('span').at(2).prop('style');
        expect(style.fontSize).toEqual(Theme.heroBanner.subtitles.fontSize);
      });
    });
  });

  describe('custom', () => {
    let wrapper;
    const backgroundImage = 'test.png';
    const backgroundColor = '#123abc';
    const textColor = '#abc123';

    beforeEach(() => {
      wrapper = renderHeroBanner({
        subtitle: 'test',
        subtitle2: 'test',
        containerStyle: {
          backgroundImage,
          backgroundColor
        },
        textStyle: {
          color: textColor
        }
      });
    });

    it('has backgroundImage set to "backgroundImage" prop', () => {
      const style = wrapper.find('div').prop('style');
      expect(style.backgroundImage).toContain(backgroundImage);
    });

    it('has backgroundColor set to "backgroundColor" prop', () => {
      const style = wrapper.find('div').prop('style');
      expect(style.backgroundColor).toContain(backgroundColor);
    });

    it('has title textColor set to "textColor" prop', () => {
      const style = wrapper.find('span').at(0).prop('style');
      expect(style.color).toEqual(textColor);
    });

    it('has subtitle1 textColor set to "textColor" prop', () => {
      const style = wrapper.find('span').at(1).prop('style');
      expect(style.color).toEqual(textColor);
    });

    it('has subtitle2 textColor set to "textColor" prop', () => {
      const style = wrapper.find('span').at(2).prop('style');
      expect(style.color).toEqual(textColor);
    });
  });

  describe('container with parentPadding', () => {
    let wrapper;
    const parentPadding = {
      top: 10,
      left: 5,
      bottom: 7
    };

    beforeEach(() => {
      wrapper = renderHeroBanner({ parentPadding });
    });

    it('has margin set to negative parentPadding', () => {
      const style = wrapper.find('div').at(0).prop('style');
      const { top, bottom, left } = parentPadding;
      const expectedMargin = `${-top}px 0px ${-bottom}px ${-left}px`;
      expect(style.margin).toEqual(expectedMargin);
    });
  });
});
