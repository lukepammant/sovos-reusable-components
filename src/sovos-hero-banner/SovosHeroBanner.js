import PropTypes from 'prop-types';
import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import heroDefaultBgImg from './assets/img/hero_bg.png';

const SovosHeroBanner = ({ title, subtitle, subtitle2, parentPadding, containerStyle, textStyle, muiTheme }) => {
  const styles = {
    container: {
      backgroundImage: heroDefaultBgImg,
      ...muiTheme.heroBanner.container,
      ...containerStyle,
    },
    title: {
      ...muiTheme.heroBanner.title,
      ...textStyle
    },
    subtitles: {
      ...muiTheme.heroBanner.subtitles,
      ...textStyle
    },
  };

  if (parentPadding) {
    const top = -parentPadding.top || 0;
    const right = -parentPadding.right || 0;
    const bottom = -parentPadding.bottom || 0;
    const left = -parentPadding.left || 0;

    styles.container.margin = `${top}px ${right}px ${bottom}px ${left}px`;
  }

  return (
    <div style={ styles.container }>
      <span style={ styles.title }>{title}</span>
      {subtitle ? <span style={ styles.subtitles }>{subtitle}</span> : null }
      {subtitle2 ? <span style={ styles.subtitles }>{subtitle2}</span> : null }
    </div>
  );
};

SovosHeroBanner.propTypes = {
  parentPadding: PropTypes.shape({
    top: PropTypes.number,
    right: PropTypes.number,
    bottom: PropTypes.number,
    left: PropTypes.number
  }),
  title: PropTypes.string,
  subtitle: PropTypes.string,
  subtitle2: PropTypes.string,
  containerStyle: PropTypes.object,
  textStyle: PropTypes.object,
  muiTheme: PropTypes.object
};

export default muiThemeable()(SovosHeroBanner);
