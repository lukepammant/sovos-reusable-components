import PropTypes from 'prop-types';
import React from 'react';
import { SelectField, MenuItem } from 'material-ui';
import getError from './errorHelper';

const SovosReduxFormSelectField = ({ input: { onChange, ...inputProps }, meta, options, ...custom }) => (
  <SelectField
    { ...inputProps }
    { ...custom }
    errorText={ getError(meta) }
    onChange={ (event, index, value) => onChange(value) }
  >
    {
      options.map(o => (
        <MenuItem
          key={ o.value }
          primaryText={ o.primaryText }
          value={ o.value }
        />
      ))
    }
  </SelectField>
);

SovosReduxFormSelectField.propTypes = {
  input: PropTypes.object,
  meta: PropTypes.object,
  options: PropTypes.array
};

export default SovosReduxFormSelectField;
