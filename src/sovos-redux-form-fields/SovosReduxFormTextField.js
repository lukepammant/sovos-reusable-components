import PropTypes from 'prop-types';
import React from 'react';
import { TextField } from 'material-ui';
import getError from './errorHelper';

const SovosReduxFormTextField = ({ input, meta, ...custom }) => (
  <TextField
    { ...input }
    { ...custom }
    errorText={ getError(meta) }
  />
);

SovosReduxFormTextField.propTypes = {
  input: PropTypes.object,
  meta: PropTypes.object
};

export default SovosReduxFormTextField;
