export default function (props) {
  return props.touched && props.error;
}
