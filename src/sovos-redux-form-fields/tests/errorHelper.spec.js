import getError from '../errorHelper';

describe('errorHelper', () => {
  describe('#getError', () => {
    it('returns falsy if the field has not been modified', () => {
      const meta = {
        touched: false,
        error: undefined
      };

      expect(getError(meta)).toBeFalsy();
    });

    it('returns falsy if the field has been modified and there is not an error', () => {
      const meta = {
        touched: true,
        error: false
      };

      expect(getError(meta)).toBeFalsy();
    });

    it('returns the error if the field has been modified and there is an error', () => {
      const error = 'test error';
      const meta = {
        touched: true,
        error
      };

      expect(getError(meta)).toBe(error);
    });
  });
});
