/* eslint-disable no-underscore-dangle */

import React from 'react';
import PropTypes from 'prop-types';
import { mount } from 'enzyme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import mockComponent from 'mockComponent';
import SovosReduxFormCheckbox, { checkboxSize } from '../SovosReduxFormCheckbox';

const checkboxComponent = 'Checkbox';
const checkboxMock = mockComponent(checkboxComponent);

const getError = 'getError';
const getErrorMock = sinon.stub();

const theme = getMuiTheme({});

function renderCheckbox(customProps = {}) {
  return mount(
    <SovosReduxFormCheckbox
      input={ {} }
      { ...customProps }
    />,
    {
      context: {
        muiTheme: theme
      },
      childContextTypes: {
        muiTheme: PropTypes.object
      }
    }
  );
}

describe('SovosReduxFormCheckbox', () => {
  let wrapper;

  beforeEach(() => {
    SovosReduxFormCheckbox.__Rewire__(checkboxComponent, checkboxMock);
    SovosReduxFormCheckbox.__Rewire__(getError, getErrorMock);
  });

  afterEach(() => {
    getErrorMock.reset();

    SovosReduxFormCheckbox.__ResetDependency__(checkboxComponent);
    SovosReduxFormCheckbox.__ResetDependency__(getError);
  });

  it('does not render an error container when the error text is empty', () => {
    wrapper = renderCheckbox();

    expect(wrapper.find('.checkbox__errorContainer')).not.toBePresent();
  });

  describe('when the error text is defined', () => {
    beforeEach(() => {
      getErrorMock.returns('Test Error');
    });

    it('renders an error container', () => {
      wrapper = renderCheckbox();

      expect(wrapper.find('.checkbox__errorContainer')).toBePresent();
    });

    it('renders the error underline with the default width if width is not specified', () => {
      wrapper = renderCheckbox();

      expect(wrapper.find('hr').prop('style').width).toBe(checkboxSize);
    });

    it('renders the error underline with the icon width if the style is specified', () => {
      const iconWidth = 50;
      wrapper = renderCheckbox({ iconStyle: { width: iconWidth } });

      expect(wrapper.find('hr').prop('style').width).toBe(iconWidth);
    });

    it('renders the error underline with the error color', () => {
      wrapper = renderCheckbox();

      expect(wrapper.find('hr').prop('style').borderColor).toBe(theme.textField.errorColor);
    });

    it('renders the error text with the error color', () => {
      wrapper = renderCheckbox();

      expect(wrapper.find('.checkbox__errorText').prop('style').color).toBe(theme.textField.errorColor);
    });
  });
});

/* eslint-enable no-underscore-dangle */
