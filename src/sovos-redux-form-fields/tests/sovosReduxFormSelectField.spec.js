/* eslint-disable no-underscore-dangle */

import React from 'react';
import { shallow } from 'enzyme';
import mockComponent from 'mockComponent';
import SovosReduxFormSelectField from '../SovosReduxFormSelectField';

const selectFieldComponent = 'SelectField';
const selectFieldMock = mockComponent(selectFieldComponent);
const menuItem = 'MenuItem';
const menuItemMock = mockComponent(menuItem);

describe('SovosReduxFormSelectField', () => {
  beforeEach(() => {
    SovosReduxFormSelectField.__Rewire__(selectFieldComponent, selectFieldMock);
    SovosReduxFormSelectField.__Rewire__(menuItem, menuItemMock);
  });

  afterEach(() => {
    SovosReduxFormSelectField.__ResetDependency__(selectFieldComponent);
    SovosReduxFormSelectField.__ResetDependency__(menuItem);
  });

  it('renders a menu item for each option', () => {
    const options = [
      { value: 1 },
      { value: 2 }
    ];

    const wrapper = shallow(
      <SovosReduxFormSelectField
        input={ {} }
        meta={ {} }
        options={ options }
      />
    );

    expect(wrapper.find(menuItem).length).toBe(options.length);
  });
});

/* eslint-enable no-underscore-dangle */
