import PropTypes from 'prop-types';
import React from 'react';
import { RadioButtonGroup } from 'material-ui';

const SovosReduxFormRadioButtonGroup = ({ input: { onChange, value, ...inputProps }, ...custom }) => (
  <RadioButtonGroup
    { ...inputProps }
    { ...custom }
    onChange={ (e, val) => onChange(val) }
    valueSelected={ value }
  />
);

SovosReduxFormRadioButtonGroup.propTypes = {
  input: PropTypes.object
};

export default SovosReduxFormRadioButtonGroup;
