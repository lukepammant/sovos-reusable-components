import PropTypes from 'prop-types';
import { createElement } from 'react';

const SovosReduxFormStaticField = ({ input: { value }, component, ...custom }) => {
  component = component || 'span';
  return createElement(component, custom, [value]);
};

SovosReduxFormStaticField.propTypes = {
  component: PropTypes.element,
  input: PropTypes.object
};

export default SovosReduxFormStaticField;
