import PropTypes from 'prop-types';
import React from 'react';
import { Checkbox } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import transitions from 'material-ui/styles/transitions';
import getError from './errorHelper';

export const checkboxSize = 24;

const rootStyle = {
  position: 'relative',
  width: '100%'
};

const errorTextStyle = {
  position: 'relative',
  top: 10,
  fontSize: 12,
  lineHeight: '12px',
  transition: transitions.easeOut()
};

const errorUnderlineStyle = {
  boxSizing: 'content-box',
  borderTop: 'none',
  borderRight: 'none',
  borderBottom: '2px solid',
  borderLeft: 'none',
  bottom: 8,
  margin: 0,
  position: 'absolute',
  transform: 'scaleX(1)',
  transition: transitions.easeOut()
};

const SovosReduxFormCheckbox = ({ input: { onChange, value, ...inputProps }, meta, muiTheme, ...custom }) => {
  let errorContainer = null;
  const errorText = getError(meta);
  const { prepareStyles, textField: { errorColor } } = muiTheme;
  let { iconStyle } = custom;
  iconStyle = iconStyle || {};

  if (errorText) {
    errorContainer = (
      <div
        className="checkbox__errorContainer"
        style={ prepareStyles(rootStyle) }
      >
        <hr style={ prepareStyles(Object.assign(errorUnderlineStyle, { borderColor: errorColor, width: iconStyle.width || checkboxSize })) } />
        <div
          className="checkbox__errorText"
          style={ prepareStyles(Object.assign(errorTextStyle, { color: errorColor })) }
        >
          { errorText }
        </div>
      </div>
    );
  }

  return (
    <div style={ prepareStyles(rootStyle) }>
      <Checkbox
        { ...inputProps }
        { ...custom }
        checked={ !!value }
        onCheck={ onChange }
      />
      { errorContainer }
    </div>
  );
};

SovosReduxFormCheckbox.propTypes = {
  input: PropTypes.object,
  meta: PropTypes.object,
  muiTheme: PropTypes.object.isRequired
};

export default muiThemeable()(SovosReduxFormCheckbox);
