import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import './sovosFileDrop.css';
import PdfIcon from './assets/img/p-d-f-icon.png';

class SovosFileDrop extends Component {

    onOpenClick() {
        this.dropzone.open();
    }

    getIcons() {
            return <img className="sovos-drag-drop__description__icon"
                        src={PdfIcon} />;
    };

    getMimeType(type) {
        switch(type) {
            case 'PDF':
                return 'application/pdf';
                break;
            case 'PNG':
                return 'image/png';
                break;
            case 'JPG':
                return 'image/jpeg';
                break;
            case 'EXCEL':
                return 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                break;
            case 'DOC':
                return 'application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document';
                break;
            case 'TEXT':
                return 'text/*';
                break;
            case 'APPLICATION':
                return 'application/*';
                break;
            case 'IMAGE':
                return 'image/*';
                break;
            case 'ZIP':
                return 'application/zip';
                break;
            case 'TIFF':
                return 'image/tiff';
                break;

            default:
                return 'application/*';
                break;
        }
    };

    getAcceptedFileTypes() {
        let acceptedTypes = this.props.acceptedTypes;
        let types = "";

        switch(typeof(acceptedTypes)) {
            case 'string':
                return this.getMimeType(acceptedTypes.toUpperCase());
                break;
            case 'undefined':
                return "";
                break;
            default:
                if(Array.isArray(acceptedTypes)) {
                    acceptedTypes.map((type, index, acceptedTypesArray) => {
                        types += this.getMimeType(type.toUpperCase());
                        if(index !== acceptedTypesArray.length - 1) {
                            types += " ,";
                        }
                    });
                } else {
                    console.error('Expected acceptedTypes to be either a string or array. Received type: ', typeof(acceptedTypes));
                }

                return types;
        }
    };

    render() {
        const activeStyle = this.props.style && this.props.style.acceptedBorderColor ?
        {border: "1px solid " + this.props.style.acceptedBorderColor} :
        {border: "1px solid #369D43"};
        const rejectStyle = this.props.style && this.props.style.rejectedBorderColor ?
        {border: "1px solid " + this.props.style.rejectedBorderColor} :
        {border: "1px solid #f93f3f"};
        const style = this.props.style && this.props.style.width && this.props.style.height ?
        {
            width: this.props.style.width || "640px",
            height: this.props.style.height || "335px"
        } : {};

        let multiple = this.props.multiple || false;

        return (
            <div>
                <Dropzone
                    style={style}
                    className="sovos-drag-drop"
                    activeStyle={activeStyle}
                    rejectStyle={rejectStyle}
                    ref={(node) => { this.dropzone = node; }}
                    onDrop={this.props.onFileSelect}
                    onDropAccepted={this.props.onDropAccepted}
                    onDropRejected={this.props.onDropRejected}
                    multiple={multiple}
                    accept={this.getAcceptedFileTypes()}>

                    <div className="sovos-drag-drop__description">
                        <div className="sovos-drag-drop--item-center">{this.getIcons()}</div>
                        <div className="sovos-drag-drop--item-center sovos-drag-drop__description--text-highlighted">Drag and Drop</div>
                        <div className="sovos-drag-drop--item-center">your files here or <span onClick={this.onOpenClick}>Browse File</span></div>
                    </div>

                </Dropzone>
            </div>
        );
    }
}

SovosFileDrop.propTypes = {
    onFileSelect : PropTypes.func.isRequired
};

export default SovosFileDrop;
