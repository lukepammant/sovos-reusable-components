import React from 'react';
import PropTypes from 'prop-types';
import { List, ListItem } from 'material-ui/List';
import muiThemeable from 'material-ui/styles/muiThemeable';
import Down from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import Up from 'material-ui/svg-icons/hardware/keyboard-arrow-up';

class NavigationLinks extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  onLinkClick = (link) => {
    const hasNestedLinks = link.nestedLinks && link.nestedLinks.length > 0;
    if (hasNestedLinks) {
      const isOpen = this.state[link.label] || false;
      this.setState({ [link.label]: !isOpen });
    } else {
      this.props.onLinkClick(link.label, link.action, hasNestedLinks);
    }
  }

  getNestedLinkArrow = (link) => {
    const style = { fill: 'white', margin: 10 };
    return this.state[link.label] ? <Up style={ style } /> : <Down style={ style } />;
  }

  getIcon = (link) => {
    const { muiTheme } = this.props;
    return React.createElement(link.icon, {
      style: muiTheme.navigation.links.icons,
      color: '#fff'
    });
  }

  renderLinkItem = (link, isNested) => {
    const { getActiveLinkStyle, muiTheme } = this.props;
    const styles = muiTheme.navigation.links;
    const hasNestedLinks = link.nestedLinks && link.nestedLinks.length > 0;
    const style = { ...styles.link, ...getActiveLinkStyle(link.label) };

    return (
      <ListItem
        className={ isNested ? 'sovos-navigation__links_nested-list-item' : 'sovos-navigation__links_list-item' }
        primaryText={ link.label }
        leftIcon={ isNested ? null : this.getIcon(link, isNested) }
        key={ link.label }
        style={ style }
        innerDivStyle={ styles.innerDivStyle }
        onClick={ () => this.onLinkClick(link) }
        primaryTogglesNestedList
        open={ this.state[link.label] }
        nestedListStyle={ styles.nestedList }
        rightIcon={ hasNestedLinks && this.getNestedLinkArrow(link) }
        nestedItems={
          link.nestedLinks &&
          link.nestedLinks.map(nestedLink => this.renderLinkItem(nestedLink, true))
        }
      />
    );
  }

  render() {
    const { links, muiTheme } = this.props;
    return (
      <div
        className="sovos-navigation__links_container"
        style={ muiTheme.navigation.links.container }
      >
        <List style={ { paddingTop: 0 } }>
          {links.map(link => this.renderLinkItem(link))}
        </List>
      </div>
    );
  }
}

const nestedLinkProps = PropTypes.shape({
  label: PropTypes.string.isRequired,
  action: PropTypes.func.isRequired,
});

export const linkPropTypes = PropTypes.shape({
  label: PropTypes.string.isRequired,
  icon: PropTypes.func.isRequired,
  action: PropTypes.func.isRequired,
  nestedLinks: PropTypes.arrayOf(nestedLinkProps),
});

NavigationLinks.propTypes = {
  muiTheme: PropTypes.object,
  getActiveLinkStyle: PropTypes.func,
  onLinkClick: PropTypes.func.isRequired,
  links: PropTypes.arrayOf(linkPropTypes).isRequired,
};

NavigationLinks.defaultProps = {
  getActiveLinkStyle: () => { },
};

export default muiThemeable()(NavigationLinks);
