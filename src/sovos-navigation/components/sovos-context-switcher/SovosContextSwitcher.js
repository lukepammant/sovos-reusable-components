import React from 'react';
import PropTypes from 'prop-types';
import Popover from 'material-ui/Popover';
import FlatButton from 'material-ui/FlatButton';
import { fade } from 'material-ui/utils/colorManipulator';
import ImageLens from 'material-ui/svg-icons/image/lens';
import ActionSearch from 'material-ui/svg-icons/action/search';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { green, pink, neutralWhite, grayMedium } from '../../../themes/sovos-colors';

const contextColors = {
  production: { background: green, icon: green, text: neutralWhite },
  test: { background: pink, icon: pink, text: neutralWhite },
  default: { background: neutralWhite, icon: grayMedium, text: 'black' }
};

class SovosContextSwitcher extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      searchValue: '',
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.selectedContext.name !== nextProps.selectedContext.name) {
      this.setState({
        open: false
      });
    }
  }

  contextMenuFilter = (context) => {
    const searchRegex = new RegExp(this.state.searchValue, 'i');
    return this.props.selectedContext.name !== context.name
      && (!this.state.searchValue || searchRegex.test(context.name));
  };

  handleContextBarClick = (event) => {
    if (this.props.contexts.length > 1) {
      this.setState({
        open: true,
        anchorEl: event.currentTarget,
        searchValue: ''
      });
    }
  };

  selectNewContext = (context) => {
    this.props.setContext(context);
  };

  closeMenu = () => {
    this.setState({ open: false });
  };

  handleSearch = (event) => {
    this.setState({ searchValue: event.target.value });
  };

  renderContextDropdownButton() {
    const { selectedContext } = this.props;
    const { background, text } = contextColors[selectedContext.type];
    const styles = this.props.muiTheme.navigation.context;

    return (
      <FlatButton
        fullWidth
        className="sovos-navigation__context-switcher_dropdown-button"
        backgroundColor={ fade(background, 0.6) }
        hoverColor={ background }
        onClick={ this.handleContextBarClick }
        style={ { height: 40, padding: '0 25px', fontWeight: 400, color: text } }
      >
        <span style={ styles.alias }>
          {selectedContext ? selectedContext.name : ''}
        </span>

        <span style={ styles.taxyear }>
          <span style={ styles.taxyearOpacity }>
            {selectedContext ? 20 : ''}
          </span>
          {selectedContext ? selectedContext.taxYear.substring(2) : ''}
        </span>
      </FlatButton>
    );
  }

  renderContextItem = (context) => {
    const styles = this.props.muiTheme.navigation.context;
    return (
      <div
        className="sovos-navigation__context-switcher_item"
        style={ styles.dropdownContext }
        key={ context.name }
        onClick={ () => this.selectNewContext(context) }
      >
        <ImageLens style={ styles.icons } color={ contextColors[context.type].icon } />
        <span
          style={ { ...styles.dropdownContextName, float: 'left' } }
        >
          {context.name}
        </span>
        <span style={ { float: 'right' } }>
          {context.taxYear}
        </span>
      </div>
    );
  }

  renderPopover() {
    const styles = this.props.muiTheme.navigation.context;
    const contextItems = this.props.contexts
      .filter(this.contextMenuFilter)
      .map(this.renderContextItem);

    return (
      <Popover
        open={ this.state.open }
        anchorEl={ this.state.anchorEl }
        onRequestClose={ this.closeMenu }
        anchorOrigin={ { horizontal: 'left', vertical: 'bottom' } }
        targetOrigin={ { horizontal: 'left', vertical: 'top' } }
      >
        <ActionSearch
          style={ styles.searchIcons }
          color="lightgrey"
        />
        <div style={ styles.search }>
          <input
            type="text"
            autoFocus
            value={ this.state.searchValue }
            onChange={ this.handleSearch }
            name="contextSearch"
            placeholder="Search"
          />
        </div>
        {contextItems}
      </Popover>
    );
  }

  render() {
    const styles = this.props.muiTheme.navigation.context;
    return (
      <div
        className="sovos-navigation__context-switcher"
        style={ styles.context }
      >
        {this.renderContextDropdownButton()}
        {this.renderPopover()}
      </div>
    );
  }
}

export const contextItemPropTypes = PropTypes.shape({
  name: PropTypes.string.isRequired,
  taxYear: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['production', 'test', 'default'])
});

SovosContextSwitcher.propTypes = {
  selectedContext: contextItemPropTypes.isRequired,
  contexts: PropTypes.arrayOf(contextItemPropTypes).isRequired,
  setContext: PropTypes.func.isRequired,
  muiTheme: PropTypes.object.isRequired,
};

export default muiThemeable()(SovosContextSwitcher);
