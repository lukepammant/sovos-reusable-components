import React from 'react';
import PropTypes from 'prop-types';
import muiThemeable from 'material-ui/styles/muiThemeable';
import SovosBranding from '../../assets/sovosbranding.png';

const SovosNavigationBranding = ({ collapsed, muiTheme, productName }) => (
  <div
    className="sovos-navigation__branding_container"
    style={ collapsed ?
      muiTheme.navigation.branding.containerCollapsed :
      muiTheme.navigation.branding.container
    }
  >
    <div
      className="sovos-navigation__branding_logo"
      style={ {
        backgroundImage: `url(${SovosBranding})`,
        ...muiTheme.navigation.branding.logo
      } }
    />
    <span
      className="sovos-navigation__branding_product-name"
      style={ muiTheme.navigation.branding.productName }
    >
      {productName}
    </span>
  </div>
);

SovosNavigationBranding.defaultProps = {
  collapsed: false
};

SovosNavigationBranding.propTypes = {
  muiTheme: PropTypes.object,
  productName: PropTypes.string.isRequired,
  collapsed: PropTypes.bool,
};

export const brandingPropTypes = SovosNavigationBranding.propTypes;

export default muiThemeable()(SovosNavigationBranding);
