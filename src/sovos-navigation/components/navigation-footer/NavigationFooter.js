import React from 'react';
import PropTypes from 'prop-types';
import muiThemeable from 'material-ui/styles/muiThemeable';

const NavigationFooter = ({ muiTheme, children }) => (
  <div
    className="sovos-navigation__footer"
    style={ muiTheme.navigation.footer }
  >
    {children}
  </div>
);

NavigationFooter.propTypes = {
  muiTheme: PropTypes.object.isRequired,
  children: PropTypes.node.isRequired
};

export default muiThemeable()(NavigationFooter);
