import React from 'react';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import ActionBuild from 'material-ui/svg-icons/action/build';
import ActionSearch from 'material-ui/svg-icons/action/search';
import ActionSettings from 'material-ui/svg-icons/action/settings';
import ActionSystemUpdateAlt from 'material-ui/svg-icons/action/system-update-alt';
import EditorFormatListBulleted from 'material-ui/svg-icons/editor/format-list-bulleted';
import FileFolder from 'material-ui/svg-icons/file/folder';
import MapsLocalPrintshop from 'material-ui/svg-icons/maps/local-printshop';
import SocialGroup from 'material-ui/svg-icons/social/group';
import SovosNavigation from './SovosNavigation';

const doNothing = () => { };

const getListItems = () => (
  [
    { name: 'Search', action: doNothing, icon: ActionSearch, expandMenu: false },
    { name: 'Import', action: null, icon: ActionSystemUpdateAlt, expandMenu: false },
    { name: 'Recipients', action: null, icon: SocialGroup, expandMenu: false },
    { name: 'Forms', action: null, icon: EditorFormatListBulleted, expandMenu: false },
    { name: 'Print', action: null, icon: MapsLocalPrintshop, expandMenu: false },
    { name: 'Transmit', action: null, icon: MapsLocalPrintshop, expandMenu: false },
    { name: 'Corrections', action: null, icon: ActionBuild, expandMenu: false },
    { name: 'Tools and Settings', action: null, icon: ActionSettings, expandMenu: true, subItems: [
      { name: 'Manage Files', second: 'manage-files', action: null, icon: FileFolder, expandMenu: false }
    ],
    },
  ]
);

const contexts = [
  {
    name: 'PROD-UNIT1',
    type: 'production',
    taxYear: '2016'
  },
  {
    name: 'TEST-UNIT1',
    type: 'test',
    taxYear: '2018'
  },
  {
    name: 'DEFAULT-UNIT1',
    type: 'default',
    taxYear: '2013'
  }
];

export default () => (
  <Card style={{ textAlign: 'center', margin: '1rem' }}>
    <CardTitle title="SideBar" />
    <CardText>
      <div>
        <SovosNavigation
          // collapsed={false}
          contexts={contexts}
          selectedContext={contexts[0]}
          branding={{ productName: '1099 ENTERPRISE' }}
          items={getListItems()}
        />
      </div>
    </CardText>
  </Card>
);
