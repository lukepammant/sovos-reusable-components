import React from 'react';
import { mount } from 'enzyme';
import ActionSearch from 'material-ui/svg-icons/action/search';
import { fade } from 'material-ui/utils/colorManipulator';
import { green, pink, neutralWhite } from '../themes/sovos-colors';
import SovosNavigation from './SovosNavigation';
import SovosThemeProvider from '../sovos-theme-provider/SovosThemeProvider';

const doNothing = () => { };
const onLinkClick = (label, action, hasNestedItems) => {
  if (!hasNestedItems) {
    action();
  }
};

const renderNavigation = (props) => {
  props = props || {};
  return mount(<SovosThemeProvider>
    <SovosNavigation
      branding={ props.branding || { productName: 'test' } }
      contexts={ props.contexts || [{ name: 'test', type: 'production', taxYear: '2018' }] }
      selectedContext={ props.selectedContext || { name: 'test', type: 'production', taxYear: '2018' } }
      links={ props.links || [{ label: 'test', action: () => { }, icon: ActionSearch }] }
      initialSelectedLink={ props.initialSelectedLink || 'test' }
      footerElements={ props.footerElements || [<ActionSearch key="abc" />] }
      setContext={ doNothing }
      hideDrawer={ doNothing }
      onLinkClick={ onLinkClick }
    />
  </SovosThemeProvider>);
};

describe('SovosNavigation', () => {
  describe('Branding', () => {
    it('shows correct branding product name', () => {
      const productName = 'blah';
      const wrapper = renderNavigation({ branding: { productName } });
      const productNameElement = wrapper.find('.sovos-navigation__branding_product-name');
      expect(productNameElement.text()).toEqual(productName);
    });
  });

  describe('Navigation Links', () => {
    const icon = ActionSearch;
    const action = doNothing;

    it('shows the links provided', () => {
      const links = [
        { label: 'a', action, icon },
        { label: 'b', action, icon },
        { label: 'c', action, icon }
      ];
      const wrapper = renderNavigation({ links });
      const linkElements = wrapper.find('.sovos-navigation__links_list-item');

      linkElements.forEach((e, i) => {
        expect(e.text()).toEqual(links[i].label);
      });
    });

    it('shows nested items when parent clicked', () => {
      const nestedLinks = [
        { label: 'b', action }
      ];
      const links = [{ label: 'a', action, icon, nestedLinks }];
      const wrapper = renderNavigation({ links });

      const initialNestedItemsCount = wrapper.find('.sovos-navigation__links_nested-list-item');
      expect(initialNestedItemsCount.exists()).toEqual(false);

      wrapper.find('.sovos-navigation__links_list-item').simulate('click');

      const nestedItemsCount = wrapper.find('.sovos-navigation__links_nested-list-item');
      expect(nestedItemsCount.exists()).toEqual(true);
    });

    it('calls action on click', () => {
      const onClick = sinon.spy();
      const links = [{ label: 'a', action: onClick, icon }];
      const wrapper = renderNavigation({ links });
      wrapper.find('.sovos-navigation__links_list-item').simulate('click');
      expect(onClick).toHaveBeenCalled();
    });

    it('set style of initially selected item', () => {
      const links = [
        { label: 'a', action, icon },
        { label: 'b', action, icon },
      ];
      const wrapper = renderNavigation({ links, initialSelectedLink: 'b' });

      const firstElement = wrapper.find('.sovos-navigation__links_list-item').at(0).prop('style');
      const selectedElement = wrapper.find('.sovos-navigation__links_list-item').at(1).prop('style');

      expect(firstElement.opacity).toEqual('0.7');
      expect(selectedElement.opacity).toEqual(1);
    });
  });

  describe('Footer', () => {
    it('shows children', () => {
      const wrapper = renderNavigation({ footerElements: [<ActionSearch key="abc" />] });
      const footerContainer = wrapper.find('.sovos-navigation__footer');
      expect(footerContainer.children().length).toEqual(1);
    });
  });

  describe('Context Switcher', () => {
    it('shows correct colors for each context type', () => {
      const contexts = [
        { name: 'test1', type: 'production', taxYear: '2018' },
        { name: 'test2', type: 'default', taxYear: '2018' },
        { name: 'test3', type: 'test', taxYear: '2018' },
      ];

      let selectedContext = contexts[0];
      let wrapper = renderNavigation({ contexts, selectedContext });
      let contextButton = wrapper.find('.sovos-navigation__context-switcher_dropdown-button');
      expect(contextButton.prop('style').backgroundColor).toEqual(fade(green, 0.6));

      selectedContext = contexts[1];
      wrapper = renderNavigation({ contexts, selectedContext });
      contextButton = wrapper.find('.sovos-navigation__context-switcher_dropdown-button');
      expect(contextButton.prop('style').backgroundColor).toEqual(fade(neutralWhite, 0.6));

      selectedContext = contexts[2];
      wrapper = renderNavigation({ contexts, selectedContext });
      contextButton = wrapper.find('.sovos-navigation__context-switcher_dropdown-button');
      expect(contextButton.prop('style').backgroundColor).toEqual(fade(pink, 0.6));
    });
  });
});
