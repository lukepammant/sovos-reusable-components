import React from 'react';
import PropTypes from 'prop-types';
import muiThemeable from 'material-ui/styles/muiThemeable';
import SovosNavigationBranding, { brandingPropTypes } from './components/navigation-branding/NavigationBranding';
import NavigationLinks, { linkPropTypes } from './components/navigation-links/NavigationLinks';
import SovosContextSwitcher, { contextItemPropTypes } from './components/sovos-context-switcher/SovosContextSwitcher';
import SideBarFooter from './components/navigation-footer/NavigationFooter';

class SovosNavigation extends React.Component {
  constructor(props) {
    super(props);

    this.state = { selectedLink: '' || props.initialSelectedLink };
  }

  getActiveLinkStyle = key => (key === this.state.selectedLink ? { opacity: 1 } : '');

  handleSelectLink = (key, action, expandMenu) => {
    action();
    this.setState({ selectedLink: key });
    if (!expandMenu) {
      this.props.hideDrawer();
    }
  }

  render() {
    const { contexts, selectedContext, setContext, collapsed, muiTheme } = this.props;
    const styles = muiTheme.navigation;

    return (
      <div
        className="sovos-navigation"
        style={ {
          ...styles.container,
          ...(collapsed ? styles.containerHidden : styles.containerOpen)
        } }
      >
        <div style={ styles.innerContainer }>
          <SovosNavigationBranding
            productName={ this.props.branding.productName }
            contexts={ this.props.contexts }
            selectedContext={ this.props.selectedContext }
            setContext={ this.props.setContext }
          />
          {contexts && contexts.length > 0 &&
            <SovosContextSwitcher
              collapsed={ collapsed }
              contexts={ contexts }
              selectedContext={ selectedContext }
              setContext={ setContext }
            />
          }
          <NavigationLinks
            links={ this.props.links }
            getActiveLinkStyle={ this.getActiveLinkStyle }
            onLinkClick={ this.handleSelectLink }
          />
          <SideBarFooter>
            {this.props.footerElements}
          </SideBarFooter>
        </div>
      </div>
    );
  }
}

SovosNavigation.defaultProps = {
  branding: { productName: '' },
  breakPoint: 960,
  collapsed: window.innerWidth < 960,
};

SovosNavigation.propTypes = {
  branding: PropTypes.shape(brandingPropTypes).isRequired,
  breakPoint: PropTypes.number,
  collapsed: PropTypes.bool,
  contexts: PropTypes.arrayOf(contextItemPropTypes).isRequired,
  footerElements: PropTypes.arrayOf(PropTypes.node),
  hideDrawer: PropTypes.func.isRequired,
  initialSelectedLink: PropTypes.string.isRequired,
  links: PropTypes.arrayOf(linkPropTypes).isRequired,
  muiTheme: PropTypes.object.isRequired,
  onLinkClick: PropTypes.func.isRequired,
  selectedContext: contextItemPropTypes.isRequired,
  setContext: PropTypes.func.isRequired,
};

export const sovosNavigationProps = SovosNavigation.propTypes;

export default muiThemeable()(SovosNavigation);
