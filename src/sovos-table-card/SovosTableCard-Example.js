import React from 'react';
import { TableHeader, TableRow, TableHeaderColumn, TableBody, TableRowColumn } from 'material-ui/Table';
import SovosTableCard from './SovosTableCard';

export default () => (
  <SovosTableCard style={ { textAlign: 'center', margin: '1rem' } }>
    <TableHeader>
      <TableRow>
        <TableHeaderColumn>Date</TableHeaderColumn>
        <TableHeaderColumn>($) Currency</TableHeaderColumn>
        <TableHeaderColumn>(%) Rate</TableHeaderColumn>
        <TableHeaderColumn>County</TableHeaderColumn>
        <TableHeaderColumn>City</TableHeaderColumn>
      </TableRow>
    </TableHeader>

    <TableBody>

      <TableRow>
        <TableRowColumn>Aug 2017</TableRowColumn>
        <TableRowColumn>500.16</TableRowColumn>
        <TableRowColumn>0.0625</TableRowColumn>
        <TableRowColumn>Middlesex</TableRowColumn>
        <TableRowColumn>Billerica</TableRowColumn>
      </TableRow>

      <TableRow>
        <TableRowColumn>Aug 2017</TableRowColumn>
        <TableRowColumn>500.16</TableRowColumn>
        <TableRowColumn>0.0625</TableRowColumn>
        <TableRowColumn>Middlesex</TableRowColumn>
        <TableRowColumn>Billerica</TableRowColumn>
      </TableRow>

      <TableRow>
        <TableRowColumn>Aug 2017</TableRowColumn>
        <TableRowColumn>500.16</TableRowColumn>
        <TableRowColumn>0.0625</TableRowColumn>
        <TableRowColumn>Middlesex</TableRowColumn>
        <TableRowColumn>Billerica</TableRowColumn>
      </TableRow>

      <TableRow>
        <TableRowColumn>Aug 2017</TableRowColumn>
        <TableRowColumn>500.16</TableRowColumn>
        <TableRowColumn>0.0625</TableRowColumn>
        <TableRowColumn>Middlesex</TableRowColumn>
        <TableRowColumn>Billerica</TableRowColumn>
      </TableRow>

    </TableBody>
  </SovosTableCard>
);
