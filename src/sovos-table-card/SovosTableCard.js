import React from 'react';
import PropTypes from 'prop-types';
import muiThemeable from 'material-ui/styles/muiThemeable';
import Card from 'material-ui/Card';
import { Table } from 'material-ui/Table';

const SovosTableCard = ({ muiTheme, cardProps, tableProps, ...otherProps }) => (
  <Card
    style={muiTheme.tableCard.card}
    { ...cardProps }
  >
    <Table
      style={muiTheme.tableCard.table}
      { ...tableProps }
    >
      {otherProps.children}
    </Table>
  </Card>
);

SovosTableCard.propTypes = {
  cardProps: PropTypes.shape(Card.propTypes),
  tableProps: PropTypes.shape(Table.propTypes)
};

SovosTableCard.defaultProps = {
  cardProps: {},
  tableProps: {}
};

export default muiThemeable()(SovosTableCard);
