import React from 'react';
import { mount } from 'enzyme';
import SovosTableCard from './SovosTableCard';
import { TableHeader, TableRow, TableHeaderColumn, TableBody, TableRowColumn } from 'material-ui/Table';
import SovosThemeProvider from '../sovos-theme-provider/SovosThemeProvider';
import Theme from '../themes/sovos-enterprise';

const renderHeroBanner = () =>
  mount(
    <SovosThemeProvider>
      <SovosTableCard>
        <TableHeader>
          <TableRow>
            <TableHeaderColumn>Date</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody>
          <TableRow>
            <TableRowColumn>Aug 2017</TableRowColumn>
          </TableRow>
        </TableBody>
      </SovosTableCard>
    </SovosThemeProvider>
  );

describe('SovosTableCard', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = renderHeroBanner();
  });

  it('has card padding-bottom set to theme padding bottom', () => {
    const style = wrapper.find('div').at(0).prop('style');
    expect(style.paddingBottom).toEqual(Theme.tableCard.card.paddingBottom);
  });
});
