import React, { PropTypes } from 'react';

export default function mockComponent(displayName) {
  const mock = (props) => {
    const { children, ...rest } = props;
    return (
      <div { ...rest }>
        { children }
      </div>
    );
  };

  mock.displayName = displayName;
  mock.propTypes = {
    children: PropTypes.any
  };

  return mock;
}
