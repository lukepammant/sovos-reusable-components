const path = require('path');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');

const root = path.resolve(__dirname, '..');
const appSrc = path.join(root, 'src');
const modulesSrc = path.join(root, 'node_modules');
const testUtilsSrc = path.join(root, 'testUtilities');

module.exports = {
  context: appSrc,

  plugins: [
    new CaseSensitivePathsPlugin()
  ],

  devtool: 'inline-source-map',

  externals: {
    cheerio: 'window',
    'react/addons': true,
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true
  },

  resolve: {
    root: [
      appSrc,
      testUtilsSrc,
      modulesSrc
    ]
  },

  sassLoader: {
    includePaths: [
      appSrc,
      modulesSrc
    ]
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel?plugins=rewire'
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        loaders: [
          'style',
          'css',
          'sass'
        ]
      },
      {
        test: /\.css?$/,
        loaders: ['style', 'css']
      },
      {
        test: /\.png$/,
        loader: 'file-loader'
      }
    ]
  }
};
