const path = require('path');
const webpackConfig = require('../webpack.test.config.js');

const root = path.resolve(__dirname, '../..');
const appSrc = path.join(root, 'src');
const modulesSrc = path.join(root, 'node_modules');
const specHelper = path.resolve(__dirname, './specHelper.js');
const specPattern = `${appSrc}/**/*.spec.js`;

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: [
      'jasmine',
      'jasmine-sinon'
    ],
    files: [
      `${modulesSrc}/babel-polyfill/dist/polyfill.js`,
      specHelper,
      {
        pattern: specPattern,
        watched: false
      }
    ],
    preprocessors: {
      [specHelper]: ['webpack'],
      [specPattern]: ['webpack']
    },
    reporters: ['mocha'],

    port: 9876,

    colors: true,

    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['jsdom'],
    captureTimeout: 60000,
    browserNoActivityTimeout: 45000,
    singleRun: false,

    webpack: webpackConfig
  });
};
