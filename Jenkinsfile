#!groovy
// ------------------------------------------------------------------------------------
// IMPORTANT:
// ------------------------------------------------------------------------------------
// Every change done to this file should follow these standards
// - Make every step efficient. If the job's full time exceeds 25 minutes, it should be
//   refactored and steps parellelized
// - If you are planning to make add/remove stages, be sure is the right way to go as
//   it will cause the whole history of jobs to stop showing up in the jenkins
// - Allways add comments that will help future people understand and maintain
//   your code
// ------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------
// General job properties
properties([[
    $class: 'CopyArtifactPermissionProperty',
    projectNames: 'promote-build'
  ], [
    $class: 'ThrottleJobProperty',
    categories: [],
    limitOneJobWithMatchingParams: false,
    maxConcurrentPerNode: 0,
    maxConcurrentTotal: 0,
    paramsToUseForLimit: '',
    throttleEnabled: false,
    throttleOption: 'project'
  ],
  pipelineTriggers([
    [$class: 'BitBucketTrigger']
  ]),
  buildDiscarder(
    logRotator(artifactDaysToKeepStr: '',
    artifactNumToKeepStr: '5',
    daysToKeepStr: '',
    numToKeepStr: '20')
  )
])


node () {
  // ------------------------------------------------------------------------------------
  // Update files from Git repository
  stage ('Checkout') {
    checkout scm
    bat 'git submodule update --init --recursive'
  }

  // ------------------------------------------------------------------------------------
  // Yarn script
  stage ('Yarn') {
    bat 'npm list -g yarn & if errorlevel 1 npm install -g yarn'
    bat 'yarn'
  }

  // ------------------------------------------------------------------------------------
  // Build script
  stage ('Build') {
    bat 'npm run build'
  }

  // ------------------------------------------------------------------------------------
  // Test script
  stage ('Test') {
    bat 'npm run test'
  }

  // ------------------------------------------------------------------------------------
  // Finally
  stage ('Finally') {
    archiveArtifacts 'dist\\**'
    emailextrecipients([
      [$class: 'DevelopersRecipientProvider'],
      [$class: 'RequesterRecipientProvider']
    ])
    step([
      $class: 'hudson.plugins.jira.JiraIssueUpdater',
      issueSelector: [$class: 'hudson.plugins.jira.selector.DefaultIssueSelector'],
      scm: [
        $class: 'GitSCM',
        branches: [[name: "*/${env.BRANCH_NAME}"]],
        userRemoteConfigs: [[url: "${env.JOB_URL}"]]
      ]
    ])
  }
}